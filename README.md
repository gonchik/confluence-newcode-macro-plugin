## This plugin has been forked for Cloud:
https://stash.atlassian.com/projects/CONFCLOUD/repos/confluence-newcode-macro-plugin/browse

## This repo is kept as Server repo:
The New Code Macro plugin (or Code Block macro) provides syntax-aware formatting for code snippets in Confluence pages. It gives Confluence users:

- Syntax formatting appropriate for widely used programming languages, like Java, C#, C++ and PHP
- IDE-style formatting and line numbering
- Customizable options for the look and feel of the code excerpt

Confluence includes the New Code Macro plugin by default. 

Prior to Confluence 3.5, Confluence shipped with a different code macro, listed on the [Atlassian Marketplace](https://marketplace.atlassian.com/plugins/confluence.macros.code). However, this New Code Macro replaces that macro and is backwards compatible with older Confluence instances.

Requirements
-------

The New Code Macro plugin requires an installation of Confluence 2.10 or higher. The Java code itself requires JRE 1.5+.

More resources
-------
For more information, see

- [Documentation](https://confluence.atlassian.com/display/DOC/Code+Block+Macro) 
- [JIRA issue tracker](https://jira.atlassian.com/browse/CONF)


Installation
-------

The New Code Macro plugin is included by default in new Confluence installations. If you need to install it manually for any reason, you can get it from its Marketplace listing:

https://marketplace.atlassian.com/plugins/com.atlassian.confluence.ext.newcode-macro-plugin

Usage
-------

To add the macro to a Confluence page, use the macro browser to insert the Code Block macro or enter the macro key {code}.

This macro offers the following configuration options: 

| Parameter | Since | Required | Description |
| --- | ---| --- | --- |
| language | 1.0 | no | (default param) the language to highlight, see below for support |
| title | 1.9 | no | Set a title for the code |
| collapse | 1.0 | no | "true" will collapse the code fragment by default |
| linenumbers | 1.0 | no | "true" will show line numbers in the left gutter (up to 1.9.2, default: true) |
| firstline | 1.0 | no | if showing line numbers, where to start numbering from (default 1) |
| theme | 1.9 | no | If set, use the specified theme instead of the default |
| macro body | 1.0 | yes | the code to format |

Add support for custom languages
-------

The implementation of the Newcode macro is based on the 2.0 version of the JavaScript based SyntaxHighlighter by Alex Gorbatchev (http://alexgorbatchev.com/wiki/). To add support for new languages, follow the steps below:

1. Create the JavaScript brush file using the description [here](http://alexgorbatchev.com/wiki/SyntaxHighlighter:Brushes:Custom).
2. Store the brush in the appropriate location
     * Jar version: the "sh/scripts" directory within the JAR file
     * Development/SVN version: the "src/main/resources/sh/scripts" directory
3. Register the brush in the atlassian-plugin.xml. 
     * This file is located in the root of the JAR file or in the "src/main/resources" directory of the development/svn version.
   * Within this XML file there is a list of default brushes and below that comments indicate where you can add your custom brushes. A brush entry always has the format below.
4. Register the brush in the languages.xml
     * This filed is located in the root of the JAR file or in the "src/main/resources" directory of the development/svn version.
    * Within this XML file there is a list of default brushes and aliases.
5. Submit a bug report within the Newcode JIRA, with type enhancement and attach the new brush file, to the [New Code JIRA project](https://ecosystem.atlassian.net/browse/NCODE). This way your new brush can be added to the list of supported languages
6. Submit the brush to the original author to get the brush included in the original distribution.

A brush entry always has the following format: 

    <builtin-language>
        <name>\[UniqueName\]</name>
        <brushSuffix>\[Suffix of the brush file stored in sh/scripts\]</brushSuffix>
        <aliases>
            <string>\[Alias 1\]</string>
            <string>\[Alias 2\]</string>
            ...
        </aliases>
    </builtin-language>


History
-------

Development of the Newcode plugin was started by Agnes Ro and Mike Cannon-Brookes. This plugin (up to version 1.03) supported Confluence v2.2 and up. Although it provided some basic functionality, there were editing issues and the Macro was not on-par with the original Code plugin. Further development wasn't done due to time constraints.

In February 2009, redevelopment was started by Jeroen Benckhuijsen. This new plugin was development from the ground up, although heavily inspired by the old code. 

The main focus points of this initial development were:

 * Solve the outstanding bugs within the current code
 * Add support for a lot more languages
 * Upgrade to a new version of the SyntaxHighlighter library
 * Support export options wihtin Confluence (e.g. export to PDF)
 * Migrate the project to the latest Maven 2 infrastructure
 * Integrate the plugin within the Confluence Plugin Repository
 * Target the newest release of Confluence and use it's features
 * Migrate to the V2 renderer framework
 * Provide a complete set of test-cases (both unit and integration test) as suggested on the Atlassian Development Network.

Todo
-------

The following improvements are scheduled to be added to the Newcode macro

* TBD

Authors
-------

Up to version 1.03

* Agnes Ro
* Mike Cannon-Brookes

As of version 1.9

* Jeroen Benckhuijsen (Project lead)