/**
 * Copyright 2009 Jeroen Benckhuijsen
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atlassian.confluence.ext.code.render;

import com.atlassian.confluence.ext.code.config.NewcodeSettings;

import java.util.Map;

import static com.atlassian.confluence.ext.code.render.Parameters.*;

/**
 * Parameter implementation of the language parameter.
 *
 * @author Jeroen Benckhuijsen
 */
public final class LanguageParameter extends MappedParameter {

    private static final Object NONE_LANG = "none";
    private static final String PLAIN_LANG = "plain";
    private static final String AS_LANG = "actionscript";
    private static final String AS3_LANG = "actionscript3";

    /**
     * Default constructor.
     *
     * @param name
     *            The name used by the macro.
     * @param mappedName
     *            The name used by the syntax highlighter
     */
    public LanguageParameter(final String name, final String mappedName) {
        super(name, mappedName);
    }

    /**
     * Determines the language to use.
     *
     * @param parameters
     *            The map of parameters.
     * @return The language to use.
     */
    @Override
    public String getValue(final Map<String, String> parameters) {
        String lang = (String) parameters.get(PARAM_LANG) == null ? (String) parameters.get(PARAM_LANGUAGE) : (String) parameters.get(PARAM_LANG);
        if (lang == null) {
            lang = (String) parameters.get(DEFAULT_PARAMETER);
            if (lang == null) {
                lang = NewcodeSettings.DEFAULT_LANGUAGE_VALUE.toLowerCase();
            }
        }

        if (NONE_LANG.equals(lang)) {
            lang = PLAIN_LANG;
        }

        if (AS_LANG.equals(lang)) {
            lang = AS3_LANG;
        }
        return lang.toLowerCase();
    }

}
