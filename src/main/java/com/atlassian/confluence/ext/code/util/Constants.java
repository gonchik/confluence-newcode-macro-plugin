/**
 * Copyright 2009 Jeroen Benckhuijsen
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atlassian.confluence.ext.code.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author Jeroen Benckhuijsen Constants for the new code macro
 */
public final class Constants {

    /**
     * The unique key for this plugin.
     */
    public static final String PLUGIN_KEY;
    public static final String OLDCODE_PLUGIN_KEY;

    static {
        InputStream is = Constants.class.getClassLoader().getResourceAsStream(
                "atlassian-plugin.properties");
        Properties props = new Properties();
        try {
            props.load(is);
        } catch (IOException e) {
            throw new IllegalStateException("Cannot load plugin properties", e);
        }
        PLUGIN_KEY = props.getProperty("atlassian.plugin.key");
        OLDCODE_PLUGIN_KEY = props.getProperty("oldcode.plugin.key");
    }

    /**
     * Private constructor.
     */
    private Constants() {
        super();
    }
}
