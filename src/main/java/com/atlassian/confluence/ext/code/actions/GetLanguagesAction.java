package com.atlassian.confluence.ext.code.actions;

import com.atlassian.confluence.core.Beanable;
import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.ext.code.languages.Language;
import com.atlassian.confluence.ext.code.languages.LanguageRegistry;
import com.opensymphony.xwork.Action;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Retrieves a list of all configured syntax highlighters from the {@link LanguageRegistry} and converts it to JSON
 * format for use in the macro browser.
 */
public class GetLanguagesAction extends ConfluenceActionSupport implements Beanable {
    private final LanguageRegistry languageRegistry;

    public GetLanguagesAction(LanguageRegistry languageRegistry) {
        this.languageRegistry = languageRegistry;
    }

    /**
     * Returns the content that will be JSONified.
     */
    public Object getBean() {
        List<Language> languages = languageRegistry.listLanguages();
        // Return the installed languages in alphabetical order.
        Collections.sort(languages, new Comparator<Language>() {
            public int compare(Language first, Language second) {
                return first.getFriendlyName().compareTo(second.getFriendlyName());
            }
        });

        return languages;
    }

    /**
     * Simple action - the magic is in {@link #getBean()}
     */
    @Override
    public String execute() throws Exception {
        return Action.SUCCESS;
    }
}