package com.atlassian.confluence.ext.code.languages.installers;

import com.atlassian.confluence.ext.code.descriptor.BrushDefinition;
import com.atlassian.confluence.ext.code.descriptor.DescriptorFacade;
import com.atlassian.confluence.ext.code.languages.DuplicateLanguageException;
import com.atlassian.confluence.ext.code.languages.InvalidLanguageException;
import com.atlassian.confluence.ext.code.languages.LanguageParser;
import com.atlassian.confluence.ext.code.languages.LanguageRegistry;
import com.atlassian.confluence.ext.code.languages.impl.BuiltinLanguage;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;

/**
 * Handles the registration of {@link com.atlassian.confluence.ext.code.languages.impl.BuiltinLanguage} instances in the {@link LanguageRegistry} once the plugin has
 * started.
 */
public class BuiltInLanguageInstaller {
    private static final Logger log = LoggerFactory.getLogger(BuiltInLanguageInstaller.class);

    private final Map<String, String> friendlyNameMap;
    private final DescriptorFacade descriptorFacade;
    private final LanguageParser languageParser;
    private final LanguageRegistry languageRegistry;

    public BuiltInLanguageInstaller(DescriptorFacade descriptorFacade, LanguageParser languageParser, LanguageRegistry languageRegistry) {
        this.descriptorFacade = descriptorFacade;
        this.languageParser = languageParser;
        this.languageRegistry = languageRegistry;

        friendlyNameMap = ImmutableMap.<String, String>builder()
                .put("AppleScript", "AppleScript")
                .put("AS3", "ActionScript")
                .put("Bash", "Bash")
                .put("ColdFusion", "ColdFusion")
                .put("Cpp", "C++")
                .put("CSharp", "C#")
                .put("CSS", "CSS")
                .put("Delphi", "Delphi")
                .put("Diff", "Diff")
                .put("Erland", "Erlang")
                .put("Groovy", "Groovy")
                .put("Java", "Java")
                .put("JavaFX", "Java FX")
                .put("JScript", "JavaScript")
                .put("Perl", "Perl")
                .put("Php", "PHP")
                .put("Plain", "Plain Text")
                .put("PowerShell", "PowerShell")
                .put("Python", "Python")
                .put("Ruby", "Ruby")
                .put("Sass", "Sass")
                .put("Scala", "Scala")
                .put("Sql", "SQL")
                .put("Vb", "Visual Basic")
                .put("Xml", "HTML and XML")
                .build();
    }

    public void onStart() {
        ClassLoader classLoader = this.getClass().getClassLoader();
        for (BrushDefinition brushDef : descriptorFacade.listBuiltinBrushes()) {
            InputStream is = null;
            try {
                is = classLoader.getResourceAsStream(brushDef.getLocation());
                InputStreamReader reader = new InputStreamReader(is, "UTF-8");
                BuiltinLanguage lang = languageParser.parseBuiltInLanguage(reader);
                lang.setFriendlyName(friendlyNameMap.get(lang.getName()));
                lang.setWebResource(brushDef.getWebResourceId());
                languageRegistry.addLanguage(lang);
            } catch (IOException e) {
                log.error(String.format("Brush definition %s could not be loaded: %s", brushDef.getLocation(), e.getMessage()), e);
            } catch (InvalidLanguageException e) {
                log.error(String.format("Brush definition %s was not a valid brush: %s", brushDef.getLocation(), e.getMessage()), e);
            } catch (DuplicateLanguageException e) {
                log.error(String.format("Brush definition %s tried to register a language or alias that was already registered: %s", brushDef.getLocation(), e.getMessage()), e);
            } finally {
                IOUtils.closeQuietly(is);
            }
        }
    }
}
