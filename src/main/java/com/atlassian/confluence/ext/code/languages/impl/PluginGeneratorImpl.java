package com.atlassian.confluence.ext.code.languages.impl;

import com.atlassian.confluence.ext.code.languages.Language;
import com.atlassian.plugin.JarPluginArtifact;
import com.atlassian.plugin.PluginArtifact;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringEscapeUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.util.Random;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Helper classes used by the {@link com.atlassian.confluence.ext.code.languages.impl.RegisteredLanguageInstallerImpl} -
 * this class does the hard work of generating a new Atlassian Plugin JAR artifact.
 * <p>
 * Most of this code is copied blatantly from similar code in the Remotable Plugins framework :-)
 */
public class PluginGeneratorImpl implements PluginGenerator {
    private static final Random RANDOMIZER = new Random();

    public PluginArtifact createPluginForLanguage(final Language languageToRegister, final Reader scriptContents) throws IOException {
        return new JarPluginArtifact(
                ZipBuilder.buildZip(getFileName(languageToRegister), new ZipHandler() {

                    public void build(ZipBuilder builder) throws IOException {
                        builder.addFile("javascript/shLang.js", scriptContents);
                        builder.addFile("atlassian-plugin.xml", getPluginXml(languageToRegister));
                    }
                })
        );
    }

    private static String getFileName(Language languageToRegister) {
        return "install-language-" + languageToRegister.getName() + "-" + RANDOMIZER.nextLong();
    }

    private static String getPluginXml(final Language languageToRegister) throws IOException {
        // Read in the template plugin descriptor
        InputStream templateResource = null;
        String templateContents = "";
        try {
            templateResource = PluginGeneratorImpl.class.getClassLoader().getResourceAsStream("templates/generator/atlassian-plugin-template.xml");
            templateContents = IOUtils.toString(templateResource);
        } finally {
            IOUtils.closeQuietly(templateResource);
        }

        // Do the substitutions
        templateContents = templateContents.replace("${language}", StringEscapeUtils.escapeXml11(languageToRegister.getName()));
        templateContents = templateContents.replace("${friendlyName}", StringEscapeUtils.escapeXml11(languageToRegister.getFriendlyName()));
        templateContents = templateContents.replace("${randomId}", StringEscapeUtils.escapeXml11(String.valueOf(RANDOMIZER.nextLong())));
        return templateContents;
    }

    interface ZipHandler {
        void build(ZipBuilder builder) throws IOException;
    }

    private static final class ZipBuilder {
        private final ZipOutputStream zout;

        public ZipBuilder(ZipOutputStream zout) {
            this.zout = zout;
        }

        public static File buildZip(String identifier, ZipHandler handler) throws IOException {
            ZipOutputStream zout = null;
            File tmpFile = null;
            try {
                tmpFile = createExtractableTempFile(identifier, ".jar");
                zout = new ZipOutputStream(new FileOutputStream(tmpFile));
                ZipBuilder builder = new ZipBuilder(zout);
                handler.build(builder);
            } finally {
                IOUtils.closeQuietly(zout);
            }
            return tmpFile;
        }


        public void addFile(String path, Reader contents) throws IOException {
            try {
                ZipEntry entry = new ZipEntry(path);
                zout.putNextEntry(entry);
                IOUtils.copy(contents, zout);
            } finally {
                IOUtils.closeQuietly(contents);
            }
        }

        public void addFile(String path, String contents) throws IOException {
            ZipEntry entry = new ZipEntry(path);
            zout.putNextEntry(entry);
            IOUtils.copy(new StringReader(contents), zout);
        }

        public static File createExtractableTempFile(String key, String suffix) throws IOException {
            return File.createTempFile(key, suffix);
        }

    }

}
