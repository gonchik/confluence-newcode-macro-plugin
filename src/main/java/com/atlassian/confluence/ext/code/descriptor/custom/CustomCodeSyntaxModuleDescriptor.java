package com.atlassian.confluence.ext.code.descriptor.custom;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;

import javax.annotation.Nonnull;

import org.dom4j.Element;

/**
 * <p>
 * Plugin module descriptor for modules that describe a JavaScript SyntaxHighlighter module for the new code macro. In
 * this way, plugins may install additional supported languages for the macro to render.
 * </p>
 * The module must describe two key pieces of information:
 * <ol>
 * <li>The unique *key* of the web resource plugin module that exposes the JavaScript SyntaxHighlighter file to the web resource framework</li>
 * <li>A friendly name or i18n key by which the file's supported language will be shown to the user.</li>
 * </ol>
 * Here's a simple snippet of atlassian-plugin.xml for a plugin containing a functioning {code}codeSyntax{code} module:
 * <pre>
 * &lt;code-syntax key='my-codesyntax-module'
 *             name='registers a SyntaxHighlighter for my language'
 *             resourceKey='custom-code-syntax'
 *             friendly-name='My Language' /&gt;
 *
 *
 * &lt;web-resource key='custom-code-syntax'
 *                  name='SyntaxHighlighter brush for my language'&gt;
 *     &lt;resource type='download' name='shLang.js' location='/javascript/shLang.js' /&gt;
 * &lt;web-resource/&gt;
 * </pre>
 */
public class CustomCodeSyntaxModuleDescriptor extends AbstractModuleDescriptor<CustomCodeSyntax> {
    private String resourceKey;
    private String friendlyName;

    public CustomCodeSyntaxModuleDescriptor(ModuleFactory moduleFactory) {
        super(moduleFactory);
    }

    @Override
    public void init(@Nonnull Plugin plugin, @Nonnull Element element) throws PluginParseException {
        super.init(plugin, element);
        resourceKey = element.attributeValue("resource-key");
        friendlyName = element.attributeValue("friendly-name");
    }

    @Override
    public CustomCodeSyntax getModule() {
        return new CustomCodeSyntaxImpl(resourceKey, friendlyName);
    }
}
