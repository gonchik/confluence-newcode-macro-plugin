/**
 * Copyright 2009 Jeroen Benckhuijsen
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atlassian.confluence.ext.code.descriptor;

import java.util.Map;

/**
 * Value object for the defintion of a theme in atlassian-plugin.xml.
 * @author Jeroen Benckhuijsen
 *
 */
public final class ThemeDefinition {

    private String location;
    private String webResourceId;
    private Map<String, String> panelLookAndFeel;

    /**
     * Default constructor.
     * @param location The location (in the classpath) of the theme file
     * @param webResourceId The Web Resource module to include for this theme
     * @param panelLookAndFeel The definition of the default panel look-and-feel
     */
    public ThemeDefinition(final String location, final String webResourceId, final Map<String, String> panelLookAndFeel) {
        super();
        this.location = location;
        this.webResourceId = webResourceId;
        this.panelLookAndFeel = panelLookAndFeel;
    }

    /**
     * @return the location
     */
    public String getLocation() {
        return location;
    }

    /**
     * @return the webResourceId
     */
    public String getWebResourceId() {
        return webResourceId;
    }

    /**
     * @return the panelLookAndFeel
     */
    public Map<String, String> getPanelLookAndFeel() {
        return panelLookAndFeel;
    }

}
