/**
 * Copyright 2009 Jeroen Benckhuijsen
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atlassian.confluence.ext.code.render;

import java.util.Map;

/**
 * Abstract implementation of a boolean parameter for which a mapping has to be made
 * between the name used by the macro and the syntax highlighter library.
 *
 * @author Jeroen Benckhuijsen
 */
public abstract class AbstractBooleanMappedParameter extends MappedParameter {

    /**
     * Default constructor.
     *
     * @param name
     *            The name used by the macro
     * @param mappedName
     *            The name used by the syntax highlighter
     */
    public AbstractBooleanMappedParameter(final String name, final String mappedName) {
        super(name, mappedName);
    }

    /**
     * Checks and returns the value for the configured parameter.
     * @param parameters The input set of parameters
     * @return The parameter value
     * @throws InvalidValueException In case the value is not valid
     */
    protected final String internalGetValue(final Map<String, String> parameters)
            throws InvalidValueException {
        String retval = super.getValue(parameters);
        if (!(null == retval || "true".equals(retval) || "false".equals(retval))) {
            throw new InvalidValueException(this.getMacroName());
        }
        return retval;
    }

}
