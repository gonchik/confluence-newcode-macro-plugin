/**
 * Copyright 2009 Jeroen Benckhuijsen
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atlassian.confluence.ext.code.languages;

import java.io.Reader;
import java.util.List;

/**
 * Registry for languages supported by this plugin.
 *
 * @author Jeroen Benckhuijsen
 *
 */
public interface LanguageRegistry {

    /**
     * Returns whether a certain alias is know to the system.
     *
     * @param alias
     *            The language alias
     * @return Whether the alias is know to the system
     */
    boolean isLanguageRegistered(String alias);

    Language getLanguage(String name) throws UnknownLanguageException;

    /**
     * Return all registered languages.
     *
     * @return All registered languages
     */
    List<Language> listLanguages();

    /**
     * Returns the web resource to load for this language.
     *
     * @param alias
     *            The language alias
     * @return The full id of the web resource
     * @throws UnknownLanguageException In case the alias is unknown
     */
    String getWebResourceForLanguage(String alias) throws UnknownLanguageException;

    /**
     * Register a new externally provided language with the registry.
     *
     * @param language
     *            A parsed language script (see {@link LanguageParser}).
     * @throws DuplicateLanguageException
     *             In case either the name or the alias was previously
     *             registered.
     */
    void addLanguage(Language language) throws DuplicateLanguageException;

    /**
     * Unregister a language previously registered with
     * {@link #addLanguage(Language)}. In case the specified language is unknown to
     * the registry (or previously unregistered), this method won't have any
     * effect. It can therefore be safely called multiple times. Also, this
     * method won't have any affect for built-in languages.
     *
     * @param name
     *            The name of the language as passed previously to
     *            registerLanguage
     */
    void unregisterLanguage(String name);
}
