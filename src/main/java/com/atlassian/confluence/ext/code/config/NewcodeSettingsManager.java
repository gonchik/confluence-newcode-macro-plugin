/**
 * Copyright 2009 Jeroen Benckhuijsen
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atlassian.confluence.ext.code.config;

import com.atlassian.confluence.ext.code.util.Constants;
import com.atlassian.confluence.setup.settings.SettingsManager;

import java.io.Serializable;
import java.util.Map;
import java.util.function.Supplier;

/**
 * The settings manager is responsible for the persistency and management of the
 * settings of the new code macro.
 *
 * @author Jeroen Benckhuijsen
 *
 */
public class NewcodeSettingsManager {

    // Injected
    private final SettingsManager settingsManager;

    private final Supplier<NewcodeSettings> currentSettings;

    /**
     * Default constructor.
     */
    public NewcodeSettingsManager(SettingsManager settingsManager) {
        this.settingsManager = settingsManager;

        this.currentSettings = () -> {
            Serializable settings = settingsManager.getPluginSettings(Constants.PLUGIN_KEY);
            NewcodeSettings result = new NewcodeSettings();
            if (settings != null && settings instanceof Map) {
                result.mapToSettings((Map<String, Object>) settings);
            }

            return result;
        };
    }

    /**
     * Returns the current settings, fetching settings from bandana
     *
     * @return The current settings
     */
    public NewcodeSettings getCurrentSettings() {
        return currentSettings.get();
    }

    /**
     * Update the persistent settings.
     *
     * @param theme
     *            The new theme
     * @param language
     *            The new language
     */
    public void updateSettings(final String theme, final String language) {
        NewcodeSettings newSettings = new NewcodeSettings();
        newSettings.setDefaultTheme(theme);
        newSettings.setDefaultLanguage(language);

        this.settingsManager.updatePluginSettings(Constants.PLUGIN_KEY,
                (Serializable) newSettings.settingsToMap());
    }
}
