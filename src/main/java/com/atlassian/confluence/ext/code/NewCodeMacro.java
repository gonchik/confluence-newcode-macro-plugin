/*
 *   Copyright 2009 Jeroen Benckhuijsen
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package com.atlassian.confluence.ext.code;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.ext.code.render.ContentFormatter;
import com.atlassian.confluence.ext.code.render.InvalidValueException;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.TokenType;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.renderer.v2.macro.basic.AbstractPanelMacro;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * A new code macro which relies on the SyntaxHighlighter JavaScript library to
 * do the actual highlighting client-side.
 *
 * @author Jeroen Benckhuijsen
 */
public final class NewCodeMacro extends AbstractPanelMacro implements Macro {

    private static final Logger LOG = LoggerFactory.getLogger(NewCodeMacro.class);

    private ContentFormatter contentFormatter = null;

    private static boolean pdlEnabled = Long.parseLong(GeneralUtil.getBuildNumber()) >= 4000;

    /**
     * Default constructor.
     */
    public NewCodeMacro() {
        super();
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.atlassian.renderer.v2.macro.basic.AbstractPanelMacro#getPanelCSSClass
     * ()
     */
    @Override
    protected String getPanelCSSClass() {
        if (pdlEnabled)
            return "code panel pdl";
        else
            return "code panel";
    }

    /*
     * (non-Javadoc)
     * 
     * @seecom.atlassian.renderer.v2.macro.basic.AbstractPanelMacro#
     * getPanelHeaderCSSClass()
     */
    @Override
    protected String getPanelHeaderCSSClass() {
        if (pdlEnabled)
            return "codeHeader panelHeader pdl";
        else
            return "codeHeader panelHeader";
    }

    /*
     * (non-Javadoc)
     * 
     * @seecom.atlassian.renderer.v2.macro.basic.AbstractPanelMacro#
     * getPanelContentCSSClass()
     */
    @Override
    protected String getPanelContentCSSClass() {
        if (pdlEnabled)
            return "codeContent panelContent pdl";
        else
            return "codeContent panelContent";
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.atlassian.renderer.v2.macro.basic.AbstractPanelMacro#getBodyRenderMode
     * ()
     */
    @Override
    public RenderMode getBodyRenderMode() {
        return RenderMode.NO_RENDER;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.atlassian.renderer.v2.macro.BaseMacro#suppressMacroRenderingDuringWysiwyg
     * ()
     */
    @Override
    public boolean suppressMacroRenderingDuringWysiwyg() {
        return false;
    }

    /**
     * Writes the actual content of this macro.
     * <ol>
     * <li>Include needed resources such as the script files and CSS files</li>
     * <li>Create the &lt;pre&gt; tag to hold the content which will be rendered
     * by the SyntaxHighlighter</li>
     * <li>Parse any parameters into something the SyntaxHighlighter can
     * understand</li>
     * </ol>
     *
     * @param parameters    The parameters entered by the user.
     * @param body          The body of the macro to render.
     * @param renderContext The render context.
     * @return The rendered content.
     * @throws MacroException In case of rendering errors.
     */
    @SuppressWarnings("unchecked")
    @Override
    public String execute(final Map parameters, final String body, final RenderContext renderContext) throws MacroException {
        try {
            return execute(parameters, body, new DefaultConversionContext(renderContext));
        } catch (MacroExecutionException e) {
            throw new MacroException(e);
        }
    }

    public String execute(Map<String, String> parameters, String body, ConversionContext conversionContext) throws MacroExecutionException {
        LOG.debug("Executing newcode macro for body: {}", body);

        // Since the 3.5.x implementation of this macro delegates to this method, where we now perform our own html encoding
        // of the macro body, we don't need the V2 renderer doing it for us anymore (which was invoked by overriding getBodyRenderMode() to return F_HTMLESCAPE)
        // This way, we avoid undesirable double html encoding (NCODE-160) of the macro body.
        //
        // CONFDEV-4893 - GeneralUtil.htmlEncode encodes ' to &#39; which while not wrong is unnecessary.
        // The javascript used by the new code macro doesn't handle numeric entities properly so we use
        // a more correct html encoding utility here instead.

        // NCODE-172 StringEscapeUtils.escapeHtml will cause multi-byte characters problem. We have to use GeneralUtil.htmlEncode here but we need to
        // replace &#39; in the javascript manually
        body = GeneralUtil.htmlEncode(body);

        String formatted;
        try {
            formatted = this.contentFormatter.formatContent(conversionContext, parameters, body);
            Map<String, String> panelParameters = contentFormatter.getPanelParametersWithThemeLayout(parameters);
            String content = super.execute(panelParameters, formatted, conversionContext.getPageContext());

            boolean collapse = StringUtils.isNotBlank(parameters.get("collapse")) && Boolean.parseBoolean(parameters.get("collapse"));
            if (collapse) {
                content = addExpandCollapseHtml(content);
            }

            LOG.debug("Newcode macro execution finished, resulting content: {}", content);

            return content;
        } catch (InvalidValueException e) {
            String text = getText("newcode.render.invalid.parameter", e.getParameter());
            return text + "<pre>" + body + "</pre>";     // NCODE-216
        } catch (MacroException e) {
            throw new MacroExecutionException(e);
        }
    }

    /**
     * Return the i18n-ed message based on the key and the parameters.
     *
     * @param key    The message key
     * @param params The message parameters
     * @return The error message
     */
    private String getText(final String key, final Object... params) {
        return ConfluenceActionSupport.getTextStatic(key, params);
    }

    /**
     * @param contentFormatter the contentFormatter to set
     */
    public void setContentFormatter(final ContentFormatter contentFormatter) {
        this.contentFormatter = contentFormatter;
    }

    public BodyType getBodyType() {
        return BodyType.PLAIN_TEXT;
    }

    public OutputType getOutputType() {
        return OutputType.BLOCK;
    }

    public TokenType getTokenType(Map map, String body, RenderContext renderContext) {
        return TokenType.BLOCK;
    }

    /*
     * to be overridden in unit test
     */
    protected void setPdlEnabled(boolean pdlEnabled) {
        this.pdlEnabled = pdlEnabled;
    }

    private String addExpandCollapseHtml(String content) {
//        content with header
//        <div class="code panel pdl" style="border-width: 1px;">
//          <div class="codeHeader panelHeader pdl" style="border-bottom-width: 1px;">
//              <b> </b>
//          </div>
//          <div class="codeContent panelContent pdl">
//              <script type="syntaxhighlighter" class="theme: Confluence; brush: js; collapse: true; gutter: false"><![CDATA[Hello World! ]]></script>
//          </div>
//        </div>

        Document doc = Jsoup.parse(content);
        Elements headerElements = doc.select("div.codeHeader");

        if (headerElements != null && StringUtils.isNotEmpty(headerElements.toString())) {
            Element headerElement = headerElements.get(0);
            headerElement.addClass("hide-border-bottom");
            headerElement.child(0).addClass("code-title");
            headerElement.child(0).after(addCollapseSourceHtml());

            Element contentElement = doc.select("div.codeContent").get(0);
            contentElement.addClass("hide-toolbar");
        } else {
            Element contentElement = doc.select("div.codeContent").get(0);
            contentElement.before(addHeaderHtml());
            contentElement.addClass("hide-toolbar");
        }

        return doc.body().html();
    }

    private String addHeaderHtml() {
        StringBuilder builder = new StringBuilder();
        builder.append("<div class=\"").append(getPanelHeaderCSSClass()).append(" hide-border-bottom").append("\">");
        builder.append("<b class='code-title'></b>");
        builder.append(addCollapseSourceHtml());
        builder.append("</div>");
        return builder.toString();
    }

    private String addCollapseSourceHtml() {
        StringBuilder builder = new StringBuilder();
        builder.append("<span class='collapse-source expand-control' style='display:none;'>");
        builder.append("<span class='expand-control-icon icon'>&nbsp;</span>");
        builder.append("<span class='expand-control-text'>");
        builder.append(getText("newcode.config.expand.source"));
        builder.append("</span></span>");
        builder.append("<span class='collapse-spinner-wrapper'></span>");
        return builder.toString();
    }

}
