package com.atlassian.confluence.ext.code.languages;

import com.atlassian.confluence.ext.code.languages.impl.BuiltinLanguage;
import com.atlassian.confluence.ext.code.languages.impl.RegisteredLanguage;

import java.io.Reader;

/**
 * Defines an interface for a component that can parse JavaScript contents and turn them into {@link Language} objects.
 * For a successful outcome, the JavaScript input should conform to the SyntaxHighlighter brush format, as defined here:
 * http://alexgorbatchev.com/SyntaxHighlighter/manual/brushes/custom.html
 * Otherwise, an {@link InvalidLanguageException} is thrown.
 */
public interface LanguageParser {
    /**
     * Parse the supplied reader contents as JavaScript and convert it into a built-in Language object (for SyntaxHighlighter
     * brushes that are pre-installed with this plugin).
     *
     * @param reader JavaScript input
     * @return A {@link Language} object
     * @throws InvalidLanguageException The input was not a valid custom brush.
     */
    BuiltinLanguage parseBuiltInLanguage(Reader reader) throws InvalidLanguageException;

    /**
     * Parse the supplied reader contents as JavaScript and convert it into a dynamically-registered Language object
     * (for SyntaxHighlighter brushes that are dynamically installed by other plugins).
     *
     * @param reader JavaScript input
     * @return A {@link Language} object
     * @throws InvalidLanguageException The input was not a valid custom brush.
     */
    RegisteredLanguage parseRegisteredLanguage(Reader reader, String friendlyName) throws InvalidLanguageException;

}
