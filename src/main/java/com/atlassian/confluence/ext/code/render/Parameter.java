/**
 * Copyright 2009 Jeroen Benckhuijsen
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atlassian.confluence.ext.code.render;

import java.util.Map;

/**
 * Base class for parameters of which the values and/or names have to be
 * translated between those understood by the macro and those understood by the
 * syntax highlighter library.
 *
 * @author Jeroen Benckhuijsen
 */
interface Parameter {

    /**
     * Returns the name of the parameter as understood by the syntax
     * highlighter.
     *
     * @return the name of the parameter as understood by the syntax highlighter
     */
    String getName();

    /**
     * Returns the name of the parameter as understood by the macro.
     *
     * @return The name of the parameter as understood by the macro
     */
    String getMacroName();

    /**
     * Returns the value of the parameter as understood by the syntax
     * highlighter library. This method may take default values, or dependent
     * values into account while trying to determine the value for the
     * parameter.
     *
     * @param parameters
     *            The map of macro parameters
     * @return The value for this parameter
     * @throws InvalidValueException
     *             In case of invalid values
     */
    String getValue(Map<String, String> parameters)
            throws InvalidValueException;

}
