/**
 * Copyright 2009 Jeroen Benckhuijsen
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atlassian.confluence.ext.code.languages;

/**
 * Thrown in case of problems with the parsing of the language file.
 *
 * @author Jeroen Benckhuijsen
 *
 */
public final class InvalidLanguageException extends Exception {

    /**
     * Serial version UID.
     */
    private static final long serialVersionUID = 1L;

    private String errorMsgKey;
    private Object[] params;

    public InvalidLanguageException(final String errorMessage, Throwable cause) {
        super(errorMessage, cause);
        this.errorMsgKey = errorMessage;
    }

    /**
     * Default constructor.
     *
     * @param errorMsgKey
     *            The message key
     * @param params
     *            The parameters of the message
     */
    public InvalidLanguageException(final String errorMsgKey,
                                    final Object... params) {
        super();
        this.errorMsgKey = errorMsgKey;
        this.params = params;
    }

    /**
     * @return the errorMsgKey
     */
    public String getErrorMsgKey() {
        return errorMsgKey;
    }

    /**
     * @return the params
     */
    public Object[] getParams() {
        return params;
    }

}
