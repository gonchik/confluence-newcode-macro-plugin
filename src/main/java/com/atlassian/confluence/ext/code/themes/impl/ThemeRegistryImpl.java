/**
 * Copyright 2009 Jeroen Benckhuijsen
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atlassian.confluence.ext.code.themes.impl;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.map.CaseInsensitiveMap;
import org.springframework.beans.factory.InitializingBean;

import com.atlassian.confluence.ext.code.descriptor.DescriptorFacade;
import com.atlassian.confluence.ext.code.descriptor.ThemeDefinition;
import com.atlassian.confluence.ext.code.themes.DuplicateThemeException;
import com.atlassian.confluence.ext.code.themes.Theme;
import com.atlassian.confluence.ext.code.themes.ThemeRegistry;
import com.atlassian.confluence.ext.code.themes.UnknownThemeException;

/**
 * Implementation of the theme registry.
 *
 * @author Jeroen Benckhuijsen
 *
 */
public final class ThemeRegistryImpl implements ThemeRegistry, InitializingBean {

    /**
     * Hash table of all themes as registered by name. Multi-thread safe.
     */
    private CaseInsensitiveMap themes = new CaseInsensitiveMap();

    private DescriptorFacade descriptorFacade = null;

    /**
     * Default constructor.
     */
    public ThemeRegistryImpl() {
        super();
    }

    /**
     * Lists the registered themes.
     *
     * @return The registered themes
     */
    @SuppressWarnings("unchecked")
    public Collection<Theme> listThemes() {
        Set<Theme> result = new HashSet<Theme>();
        result.addAll(themes.values());
        return result;
    }

    /**
     * Register a theme with the registry.
     *
     * @param theme
     *            The theme to register.
     * @throws DuplicateThemeException In case of a duplicate theme
     */
    void registerTheme(final Theme theme) throws DuplicateThemeException {
        if (themes.get(theme.getName()) != null) {
            throw new DuplicateThemeException("newcode.theme.register.duplicate.name", theme.getName());
        }
        themes.put(theme.getName(), theme);
    }

    /**
     * {@inheritDoc}
     */
    public boolean isThemeRegistered(final String theme) {
        return themes.containsKey(theme);
    }

    /**
     * {@inheritDoc}
     */
    public String getWebResourceForTheme(final String name) throws UnknownThemeException {
        Theme theme = (Theme) themes.get(name);
        if (theme == null) {
            throw new UnknownThemeException(name);
        }
        return theme.getWebResource();
    }

    /**
     * {@inheritDoc}
     */
    public Map<String, String> getThemeLookAndFeel(String name) throws UnknownThemeException {
        Theme theme = (Theme) themes.get(name);
        if (theme == null) {
            throw new UnknownThemeException(name);
        }
        return theme.getDefaultLayout();
    }

    /**
     * {@inheritDoc}
     */
    public void afterPropertiesSet() throws Exception {
        ThemeDefinition[] builtins = descriptorFacade.listBuiltinThemes();
        for (ThemeDefinition themeDef : builtins) {
            String location = themeDef.getLocation();
            int start = "sh/styles/shTheme".length();
            int end = location.length() - ".css".length();
            String name = location.substring(start, end);
            BuiltinTheme theme = new BuiltinTheme(name, location, themeDef.getPanelLookAndFeel());
            theme.setWebResource(themeDef.getWebResourceId());
            registerTheme(theme);
        }

    }

    /**
     * @param descriptorFacade the descriptorFacade to set
     */
    public void setDescriptorFacade(final DescriptorFacade descriptorFacade) {
        this.descriptorFacade = descriptorFacade;
    }

}
