/**
 * Copyright 2009 Jeroen Benckhuijsen
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atlassian.confluence.ext.code.themes;

import java.util.Collection;
import java.util.Map;

/**
 * Registry for themes supported by this plugin.
 *
 * @author Jeroen Benckhuijsen
 *
 */
public interface ThemeRegistry {

    /**
     * Returns whether a certain theme is know to the system.
     *
     * @param theme
     *            The theme name
     * @return Whether the theme is know to the system
     */
    boolean isThemeRegistered(String theme);

    /**
     * List all registered themes.
     *
     * @return the names of the registered themes
     */
    Collection<Theme> listThemes();

    /**
     * Returns the full web resource id for a theme.
     * @param name The name of the theme
     * @return The web resource id to include
     * @throws UnknownThemeException In case of an unknown theme
     */
    String getWebResourceForTheme(String name) throws UnknownThemeException;

    /**
     * Returns the look and feel for a theme.
     * @param name The name of the theme
     * @return The look and feel for the theme
     * @throws UnknownThemeException In case of an unknown theme
     */
    Map<String, String> getThemeLookAndFeel(String name) throws UnknownThemeException;
}
