/**
 * Copyright 2009 Jeroen Benckhuijsen
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atlassian.confluence.ext.code.config;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.ext.code.languages.DuplicateLanguageException;
import com.atlassian.confluence.ext.code.languages.InvalidLanguageException;
import com.atlassian.confluence.ext.code.languages.Language;
import com.atlassian.confluence.ext.code.languages.LanguageRegistry;
import com.atlassian.confluence.ext.code.languages.RegisteredLanguageInstaller;

import com.atlassian.confluence.ext.code.languages.UnknownLanguageException;
import com.atlassian.confluence.ext.code.themes.Theme;
import com.atlassian.confluence.ext.code.themes.ThemeRegistry;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.xwork.FileUploadUtils;
import com.opensymphony.webwork.ServletActionContext;
import com.opensymphony.webwork.dispatcher.multipart.MultiPartRequestWrapper;
import com.opensymphony.xwork.Action;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * XWork action to handle all configuration of the New Code macro.
 *
 * @author Jeroen Benckhuijsen
 *
 */
public final class ConfigureNewcodeAction extends ConfluenceActionSupport {

    private static final Logger LOG = LoggerFactory.getLogger(ConfigureNewcodeAction.class);

    // if you change this, also need to change client-side validation in newcode-templates.soy and corresponding validation messages in newcode.properties
    private static final int MAX_LANGUAGE_NAME_LENGTH = 30;

    /*
     * Properties
     */
    private String defaultThemeName;
    private String defaultLanguageName;
    private String newLanguageName;

    /*
     * Injected components
     */
    private NewcodeSettingsManager newcodeSettingsManager;
    private ThemeRegistry themeRegistry;
    private LanguageRegistry languageRegistry;
    private RegisteredLanguageInstaller languageInstaller;

    public void setNewcodeSettingsManager(NewcodeSettingsManager newcodeSettingsManager) {
        this.newcodeSettingsManager = newcodeSettingsManager;
    }

    public void setThemeRegistry(ThemeRegistry themeRegistry) {
        this.themeRegistry = themeRegistry;
    }

    public void setLanguageInstaller(RegisteredLanguageInstaller languageInstaller) {
        this.languageInstaller = languageInstaller;
    }

    public void setLanguageRegistry(LanguageRegistry languageRegistry) {
        this.languageRegistry = languageRegistry;
    }


    /**
     * Default constructor.
     */
    public ConfigureNewcodeAction() {
        super();
    }

    public boolean getDisplayUpload() {
        return permissionManager.hasPermission(getRemoteUser(), Permission.ADMINISTER, PermissionManager.TARGET_SYSTEM);
    }

    /**
     * @return the defaultLanguage
     */
    public Language getDefaultLanguage() throws UnknownLanguageException // Should only ever get thrown if the default, built-in language couldn't be retrieved, in which case WTF?
    {
        String defaultLanguage = this.newcodeSettingsManager.getCurrentSettings().getDefaultLanguage();
        if (!StringUtils.isBlank(defaultLanguage)) {
            try {
                return languageRegistry.getLanguage(defaultLanguage);
            } catch (UnknownLanguageException e) {
                // Fall back to the default behaviour below; this probably means that the
                // default language was un-installed.
                LOG.warn("Unable to retrieve default language {}; has it been removed?", defaultLanguage);
            }
        }
        return languageRegistry.getLanguage(NewcodeSettings.DEFAULT_LANGUAGE_VALUE);
    }

    public String getDefaultLanguageName() throws Exception {
        return getDefaultLanguage().getName();
    }

    /**
     * Returns the default Code Macro language. If not default language has been selected
     * by the Administrator, the hard-coded default Language is used.
     */
    public String getDefaultLanguageAlias() throws Exception {
        Language defaultLanguage = getDefaultLanguage();
        return defaultLanguage.getAliases().iterator().next();
    }

    /**
     * Returns the Web Resource Key of the default Theme. If no theme has been selected by
     * the administrator, the hard-coded default Theme is used.
     */
    public String getDefaultThemeResource() throws Exception {
        String defaultThemeName = newcodeSettingsManager.getCurrentSettings().getDefaultTheme();
        if (StringUtils.isBlank(defaultThemeName))
            defaultThemeName = NewcodeSettings.DEFAULT_THEME_VALUE;

        return themeRegistry.getWebResourceForTheme(defaultThemeName);
    }

    /**
     * Returns the fully-qualified Web Resource key for the default Code Macro language. If no
     * default language has been selected by the administrator, the hard-coded default language
     * is used.
     */
    public String getDefaultLanguageResource() throws Exception {
        Language language = languageRegistry.getLanguage(getDefaultLanguageAlias());
        return languageRegistry.getWebResourceForLanguage(language.getAliases().iterator().next());
    }

    /**
     * Called to set the defaults.
     *
     * @return The result of the action
     */
    public String input() {
        return INPUT;
    }

    /**
     * Called when the user presses the save button.
     *
     * @return success or failure
     * @throws Exception In case the save action fails
     */
    public String save() throws Exception {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Handling save event for the Newcode configuration UI");
        }

        if ("".equals(defaultThemeName)) {
            defaultThemeName = null;
        }

        if ("".equals(defaultLanguageName)) {
            defaultLanguageName = null;
        }

        this.newcodeSettingsManager.updateSettings(defaultThemeName, defaultLanguageName);
        addActionMessage(getText("newcode.config.successfully.saved"));

        if (LOG.isDebugEnabled()) {
            LOG.debug("Save event for the Newcode configuration UI handled");
        }

        return SUCCESS;
    }

    /**
     * Action to handle adding a language.
     *
     * @return success or failure
     */
    public String addLanguage() throws Exception {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Handling addLanguage event for the Newcode configuration UI");
        }

        if (!permissionManager.hasPermission(AuthenticatedUserThreadLocal.getUser(), Permission.ADMINISTER, PermissionManager.TARGET_SYSTEM)) {
            LOG.error("Only system administrators may add new code macro languages.");
            addActionError(getText("newcode.config.language.add.sysadmin.required"));
            return Action.ERROR;
        }

        if (StringUtils.isBlank(newLanguageName)) {
            addActionError(getText("newcode.config.language.add.friendlyname.required"));
            return Action.INPUT;
        }

        if (newLanguageName.length() > MAX_LANGUAGE_NAME_LENGTH) {
            addActionError(getText("newcode.config.language.add.friendlyname.length"));
            return Action.INPUT;
        }

        // Make sure this request was a multi-part form POST. Otherwise FileUploadUtils is going to throw an ugly
        // exception.
        if (!(ServletActionContext.getRequest() instanceof MultiPartRequestWrapper)) {
            addActionError(getText("newcode.config.language.add.filename.required"));
            return Action.INPUT;
        }
        File uploadedLanguage = FileUploadUtils.getSingleFile();
        if (uploadedLanguage == null) {
            addActionError(getText("newcode.config.language.add.filename.required"));
            return Action.INPUT;
        }

        FileReader reader = new FileReader(uploadedLanguage);
        try {
            languageInstaller.installLanguage(reader, newLanguageName);
        } catch (InvalidLanguageException e) {
            addActionError(getText("newcode.config.language.invaliddefinition"));
            return Action.ERROR;
        } catch (DuplicateLanguageException e) {
            addActionError(getText("newcode.config.language.duplicatelanguage", new Object[]{e.getLanguageNameInError()}));
            return Action.ERROR;
        } finally {
            reader.close();
        }

        addActionMessage(getText("newcode.config.successfully.added"));
        return Action.SUCCESS;
    }

    /**
     * {@inheritDoc}
     */
    public String getActionName(final String fullClassName) {
        // TODO: Check i18n
        return "Configure New Code Plugin";
    }

    /**
     * @return the list of selectable themes
     */
    public List<String> getThemes() {
        Collection<Theme> themes = themeRegistry.listThemes();
        List<String> result = new ArrayList<String>();

        for (Theme theme : themes) {
            result.add(theme.getName());
        }
        Collections.sort(result);

        return result;
    }

    /**
     * @return the list of selectable languages
     */
    public List<Language> getLanguages() {
        List<Language> languages = languageRegistry.listLanguages();
        Collections.sort(languages, new Comparator<Language>() {
            public int compare(Language first, Language second) {
                String firstName = StringUtils.isBlank(first.getFriendlyName()) ? first.getName() : first.getFriendlyName();
                String secondName = StringUtils.isBlank(second.getFriendlyName()) ? second.getName() : second.getFriendlyName();
                return firstName.compareTo(secondName);
            }
        });
        return languages;
    }

    /**
     * @return the defaultTheme
     */
    public String getCurrentDefaultThemeName() {
        String defaultTheme = this.newcodeSettingsManager.getCurrentSettings().getDefaultTheme();
        if (StringUtils.isBlank(defaultTheme))
            return NewcodeSettings.DEFAULT_THEME_VALUE;
        else
            return defaultTheme;
    }

    /**
     * @param defaultThemeName
     *            the defaultTheme to set
     */
    public void setDefaultThemeName(final String defaultThemeName) {
        this.defaultThemeName = defaultThemeName;
    }

    /**
     * @param defaultLanguageName the defaultLanguage to set
     */
    public void setDefaultLanguageName(final String defaultLanguageName) {
        this.defaultLanguageName = defaultLanguageName;
    }

    public void setNewLanguageName(String newLanguageName) {
        this.newLanguageName = newLanguageName;
    }
}
