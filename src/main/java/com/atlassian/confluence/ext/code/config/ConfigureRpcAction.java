package com.atlassian.confluence.ext.code.config;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.ext.code.descriptor.custom.CustomCodeSyntaxModuleDescriptor;
import com.atlassian.confluence.ext.code.languages.Language;
import com.atlassian.confluence.ext.code.languages.LanguageRegistry;
import com.atlassian.confluence.ext.code.languages.UnknownLanguageException;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginController;
import com.opensymphony.xwork.Action;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Exposes simple RPC methods for configuring the Newcode Macro Plugin, for testing purposes.
 * <p>
 * TODO: Someone more adventurous than me should consider removing this Action and instead re-writing
 * some of the plugin's tests to use the new "Wired Test Runner" in the Plugin SDK.
 */
public class ConfigureRpcAction extends ConfluenceActionSupport {
    private static final Logger log = LoggerFactory.getLogger(ConfigureRpcAction.class);

    private PluginAccessor pluginAccessor;
    private PluginController pluginController;
    private LanguageRegistry languageRegistry;

    private String languageName;

    public void setPluginAccessor(PluginAccessor pluginAccessor) {
        this.pluginAccessor = pluginAccessor;
    }

    public void setPluginController(PluginController pluginController) {
        this.pluginController = pluginController;
    }

    public void setLanguageRegistry(LanguageRegistry languageRegistry) {
        this.languageRegistry = languageRegistry;
    }

    @Override
    public boolean isPermitted() {
        return permissionManager.hasPermission(AuthenticatedUserThreadLocal.getUser(), Permission.ADMINISTER, PermissionManager.TARGET_SYSTEM);
    }

    public void setLanguageName(String languageName) {
        this.languageName = languageName;
    }

    public String removeLanguage() {
        if (StringUtils.isBlank(languageName))
            return Action.INPUT;

        try {
            Language language = languageRegistry.getLanguage(languageName);
            if (language.isBuiltIn()) {
                log.error(String.format("Built-in language %s cannot be un-installed.", languageName));
                return Action.ERROR;
            }

            String pluginKey = getPluginKeyForLanguage(language);
            Plugin plugin = pluginAccessor.getPlugin(pluginKey);
            pluginController.uninstall(plugin);
        } catch (UnknownLanguageException e) {
            log.warn(String.format("Language %s does not exist in the Code macro configuration", languageName));
        }

        return Action.SUCCESS;
    }

    private String getPluginKeyForLanguage(Language language) {
        return language.getWebResource().substring(0, language.getWebResource().indexOf(":"));
    }
}
