/**
 * Copyright 2009 Jeroen Benckhuijsen
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atlassian.confluence.ext.code.render;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import static com.atlassian.confluence.ext.code.render.Parameters.*;

/**
 * The parameter mapper is responsible for transforming the macro parameters
 * into a string which can be included in the HTML output generated by the
 * macro.
 *
 * @author Jeroen Benckhuijsen
 */
class ParameterMapper {

    /**
     * Parameter mapping, maps parameters used by the plugin to parameters which
     * are understood by the SyntaxHighlighter library.
     */
    private final Map<String, Parameter> paramMap;

    /**
     * Default constructor.
     */
    ParameterMapper() {
        super();

        paramMap = new HashMap<String, Parameter>();

        addParameter(new LanguageParameter(PARAM_LANG, "brush"));
        addParameter(new BooleanMappedParameter(PARAM_COLLAPSE, PARAM_COLLAPSE));
        addParameter(new IntegerMappedParameter(PARAM_FIRSTLINE, "first-line"));
        addParameter(new LineNumbersParameter(PARAM_LINENUMBERS, "gutter"));
        addParameter(new ThemeParameter(PARAM_THEME, "theme"));
        addParameter(new BooleanMappedParameter(PARAM_EXPORTIMAGE,
                PARAM_EXPORTIMAGE));
    }

    /**
     * Add a parameter to the param map.
     *
     * @param parameter
     *            The parameter to add
     */
    private void addParameter(final Parameter parameter) {
        paramMap.put(parameter.getMacroName(), parameter);
    }

    /**
     * Returns the value for the language parameter.
     *
     * @param parameters
     *            The macro parameters
     * @return the value for the language parameter
     * @throws InvalidValueException
     *             In case of an invalid value specified
     */
    String getLanguage(final Map<String, String> parameters)
            throws InvalidValueException {
        return paramMap.get(PARAM_LANG).getValue(parameters) == null ? paramMap.get(PARAM_LANGUAGE).getValue(parameters) : paramMap.get(PARAM_LANG).getValue(parameters);
    }

    /**
     * Returns the value for the theme parameter.
     *
     * @param parameters
     *            The macro parameters
     * @return the value for the theme parameter
     * @throws InvalidValueException
     *             In case of an invalid value specified
     */
    String getTheme(final Map<String, String> parameters)
            throws InvalidValueException {
        return paramMap.get(PARAM_THEME).getValue(parameters);
    }

    /**
     * Returns the value for the exportImage parameter.
     *
     * @param parameters
     *            The macro parameters
     * @return the value for the exportImage parameter
     * @throws InvalidValueException
     *             In case of an invalid value specified
     */
    Boolean getExportImage(final Map<String, String> parameters)
            throws InvalidValueException {
        String value = paramMap.get(PARAM_EXPORTIMAGE).getValue(parameters);
        return (value != null) ? Boolean.valueOf(value) : null;
    }

    /**
     * Maps the parameters from those understood by the macro to those
     * understood by the syntax highlighter library.
     *
     * @param parameters
     *            The input set of parameters
     * @return the mapped parameters
     * @throws InvalidValueException
     *             In case of an invalid language specified
     */
    Map<String, String> mapParameters(final Map<String, String> parameters)
            throws InvalidValueException {
        Map<String, String> result = new HashMap<String, String>();

        for (Entry<String, Parameter> e : paramMap.entrySet()) {
            Parameter param = e.getValue();
            String value = param.getValue(parameters);
            if (value != null) {
                result.put(param.getName(), value);
            }
        }
        return result;
    }
}
