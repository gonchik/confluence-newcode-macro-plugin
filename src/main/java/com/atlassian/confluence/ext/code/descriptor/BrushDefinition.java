/**
 * Copyright 2009 Jeroen Benckhuijsen
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atlassian.confluence.ext.code.descriptor;

/**
 * Value object for the defintion of a brush in atlassian-plugin.xml.
 * @author Jeroen Benckhuijsen
 *
 */
public final class BrushDefinition {

    private String location;
    private String webResourceId;

    /**
     * Default constructor.
     * @param location The location (in the classpath) of the brush file
     * @param webResourceId The Web Resource module to include for this brush
     */
    public BrushDefinition(final String location, final String webResourceId) {
        super();
        this.location = location;
        this.webResourceId = webResourceId;
    }

    /**
     * @return the location
     */
    public String getLocation() {
        return location;
    }

    /**
     * @return the webResourceId
     */
    public String getWebResourceId() {
        return webResourceId;
    }

}
