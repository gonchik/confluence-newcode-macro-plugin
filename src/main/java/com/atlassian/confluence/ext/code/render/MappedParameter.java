/**
 * Copyright 2009 Jeroen Benckhuijsen
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atlassian.confluence.ext.code.render;

import java.util.Map;

/**
 * Implementation of a parameter for which a mapping has to be made between the
 * name used by the macro and the syntax highlighter library.
 *
 * @author Jeroen Benckhuijsen
 */
class MappedParameter implements Parameter {
    private String name;
    private String mappedName;

    /**
     * Default constructor.
     *
     * @param name
     *            The name used by the macro
     * @param mappedName
     *            The name used by the syntax highlighter
     */
    MappedParameter(final String name, final String mappedName) {
        super();
        this.name = name;
        this.mappedName = mappedName;
    }

    /**
     * {@inheritDoc}
     */
    public final String getName() {
        return mappedName;
    }

    /**
     * {@inheritDoc}
     */
    public final String getMacroName() {
        return name;
    }

    /**
     * {@inheritDoc}
     */
    public String getValue(final Map<String, String> parameters)
            throws InvalidValueException {
        return parameters.get(name);
    }

}
