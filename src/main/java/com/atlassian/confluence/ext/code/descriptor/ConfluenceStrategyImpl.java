/**
 * Copyright 2009 Jeroen Benckhuijsen
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atlassian.confluence.ext.code.descriptor;

import com.atlassian.confluence.ext.code.util.Constants;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.elements.ResourceDescriptor;
import org.apache.commons.lang3.StringUtils;

import java.util.*;

/**
 * {@link ConfluenceStrategy} compatible with Confluence 2.10 and higher.
 * @author Jeroen Benckhuijsen 
 *
 */
public class ConfluenceStrategyImpl implements ConfluenceStrategy {

    private static final String THEME_DESCRIPTOR_PREFIX = "sh-theme-";
    private static final String LAYOUT_PREFIX = "layout-";
    private static final String LOCALIZATION_DESCRIPTOR_PREFIX = "syntaxhighlighter-lang-";

    private PluginAccessor pluginAccessor;

    /**
     * Default constructor.
     * @param pluginAccessor
     *            The accessor used to interact with confluence
     */
    public ConfluenceStrategyImpl(final PluginAccessor pluginAccessor) {
        super();
        this.pluginAccessor = pluginAccessor;
    }

    /**
     * {@inheritDoc}
     */
    public BrushDefinition[] listBuiltinBrushes() {
        ModuleDescriptor<?> descriptor = getDescriptor("syntaxhighlighter-brushes");
        List<ResourceDescriptor> resources = descriptor.getResourceDescriptors();
        BrushDefinition[] result = new BrushDefinition[resources.size()];
        int i = 0;
        for (ResourceDescriptor resource : resources) {
            result[i] = new BrushDefinition(resource.getLocation(), descriptor.getCompleteKey());
            i++;
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    public ThemeDefinition[] listBuiltinThemes() {
        Plugin plugin = pluginAccessor.getPlugin(Constants.PLUGIN_KEY);
        Collection<ModuleDescriptor<?>> descriptors = plugin.getModuleDescriptors();

        List<ThemeDefinition> result = new ArrayList<ThemeDefinition>();
        for (ModuleDescriptor<?> descriptor : descriptors) {
            if (descriptor.getKey().startsWith(THEME_DESCRIPTOR_PREFIX)) {
                List<ResourceDescriptor> resources = descriptor.getResourceDescriptors();
                ResourceDescriptor first = resources.get(0);
                String location = first.getLocation();
                String webResourceId = descriptor.getCompleteKey();

                // Get panel look and feel
                Map<String, String> panelLookAndFeel = new HashMap<String, String>();
                for (Map.Entry<String, String> param : descriptor.getParams().entrySet()) {
                    if (param.getKey().startsWith(LAYOUT_PREFIX)) {
                        panelLookAndFeel.put(param.getKey(), param.getValue());
                    }
                }

                result.add(new ThemeDefinition(location, webResourceId, panelLookAndFeel));
            }
        }

        return result.toArray(new ThemeDefinition[result.size()]);
    }

    public List<String> listLocalization() {
        Plugin plugin = pluginAccessor.getPlugin(Constants.PLUGIN_KEY);
        Collection<ModuleDescriptor<?>> descriptors = plugin.getModuleDescriptors();

        List<String> result = new ArrayList<String>();
        for (ModuleDescriptor<?> descriptor : descriptors) {
            if (descriptor.getKey().startsWith(LOCALIZATION_DESCRIPTOR_PREFIX)) {
                String webResourceId = descriptor.getCompleteKey();
                if (StringUtils.isNotEmpty(webResourceId)) {
                    String languageKey = webResourceId.substring(webResourceId.length() - 2, webResourceId.length());
                    result.add(languageKey.toLowerCase());
                }
            }
        }

        return result;
    }

    /**
     * Returns a module descriptor identified by key.
     * @param key The key of the module descriptor
     * @return The module descriptor
     */
    protected ModuleDescriptor<?> getDescriptor(final String key) {
        return pluginAccessor.getPlugin(Constants.PLUGIN_KEY)
                .getModuleDescriptor(key);
    }
}
