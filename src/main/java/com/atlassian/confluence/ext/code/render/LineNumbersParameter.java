/**
 * Copyright 2009 Jeroen Benckhuijsen
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atlassian.confluence.ext.code.render;

import java.util.Map;

/**
 * Parameter implementation for the "first-line" parameter.
 *
 * @author Jeroen Benckhuijsen
 */
public final class LineNumbersParameter extends AbstractBooleanMappedParameter {

    /**
     * Default constructor.
     *
     * @param name
     *            The name used by the macro
     * @param mappedName
     *            The name used by the syntax highlighter
     */
    public LineNumbersParameter(final String name, final String mappedName) {
        super(name, mappedName);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.atlassian.confluence.ext.code.render.BooleanMappedParameter#getValue
     * (java.util.Map)
     */
    @Override
    public String getValue(final Map<String, String> parameters)
            throws InvalidValueException {
        String value = internalGetValue(parameters);
        // Default to false (which contradicts with the SyntaxHighlighter
        // library
        // to ensure conformance with the old code macro
        return (value == null) ? "false" : value;
    }

}
