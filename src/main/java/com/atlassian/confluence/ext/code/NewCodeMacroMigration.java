package com.atlassian.confluence.ext.code;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.xhtml.MacroMigration;
import com.atlassian.confluence.xhtml.api.MacroDefinition;

/**
 * newcode is an alias for the code macro. 'newcode' is no longer supported in Confluence 4.0 and so this
 * class migrates any instances of 'newcode' to 'code'.
 */
public class NewCodeMacroMigration implements MacroMigration {
    public MacroDefinition migrate(MacroDefinition macro, ConversionContext context) {
        macro.setName("code");
        return macro;
    }
}
