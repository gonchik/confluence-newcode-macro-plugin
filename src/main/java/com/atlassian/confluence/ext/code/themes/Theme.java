/**
 * Copyright 2009 Jeroen Benckhuijsen
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atlassian.confluence.ext.code.themes;

import java.util.Map;

/**
 * Interface for the registration of supported languages.
 *
 * @author Jeroen Benckhuijsen
 *
 */
public interface Theme {

    /**
     * Returns the name of the theme.
     *
     * @return The name of the name
     */
    String getName();

    /**
     * Returns the URL to the stylesheet.
     *
     * @return The URL to the stylehseet
     */
    String getStyleSheetUrl();

    /**
     * Returns whether this theme is built-in.
     *
     * @return whether this theme is built-in
     */
    boolean isBuiltIn();

    /**
     * @return the id of the web resource to include for this theme
     */
    String getWebResource();

    /**
     * @return the properties for the panel component which describe the default layout
     */
    Map<String, String> getDefaultLayout();
}
