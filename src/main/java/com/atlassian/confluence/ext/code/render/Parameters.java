/**
 * Copyright 2009 Jeroen Benckhuijsen
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atlassian.confluence.ext.code.render;

/**
 * Contains various constants for parameters.
 *
 * @author Jeroen Benckhuijsen
 */
final class Parameters {
    /*
     * Parameters
     */
    static final String DEFAULT_PARAMETER = "0";

    // Supported by the 1.0 version of NewCode macro
    static final String PARAM_LANG = "lang";
    static final String PARAM_LANGUAGE = "language";
    static final String PARAM_COLLAPSE = "collapse";
    static final String PARAM_FIRSTLINE = "firstline";
    static final String PARAM_LINENUMBERS = "linenumbers";

    // Added parameters
    static final String PARAM_THEME = "theme";
    static final String PARAM_EXPORTIMAGE = "exportImage";

    // Parameters inherited by the panel
    static final String[] PANEL_PARAMS = {
            "title",
            "borderStyle",
            "borderColor",
            "borderWidth",
            "bgColor",
            "titleBGColor"
    };

    /**
     * Fake parameter to indicate exporting.
     */
    static final String PARAM_EXPORT = "_isExport";

    /**
     * Private constructor.
     */
    private Parameters() {
    }
}
