/**
 * Copyright 2009 Jeroen Benckhuijsen
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atlassian.confluence.ext.code.languages.impl;

import com.atlassian.confluence.ext.code.languages.DuplicateLanguageException;
import com.atlassian.confluence.ext.code.languages.Language;
import com.atlassian.confluence.ext.code.languages.LanguageRegistry;
import com.atlassian.confluence.ext.code.languages.UnknownLanguageException;
import com.google.common.collect.Lists;

import java.util.*;

/**
 * Implementation of the language registry.
 *
 * @author Jeroen Benckhuijsen
 *
 */
public final class LanguageRegistryImpl implements LanguageRegistry {

    /**
     * Hash table of all languages as registered by alias. Multi-thread safe.
     */
    private Map<String, Language> languages = new Hashtable<String, Language>();
    private Map<String, Language> languagesByName = new HashMap<String, Language>();

    /**
     * {@inheritDoc}
     */
    public boolean isLanguageRegistered(final String alias) {
        return languages.containsKey(alias);
    }

    public Language getLanguage(String name) throws UnknownLanguageException {
        Language language = languagesByName.get(name);
        if (language == null)
            language = languages.get(name);

        if (language == null)
            throw new UnknownLanguageException(name);

        return language;
    }

    /**
     * {@inheritDoc}
     */
    public String getWebResourceForLanguage(final String alias)
            throws UnknownLanguageException {
        Language lang = languages.get(alias);
        if (lang == null) {
            throw new UnknownLanguageException(alias);
        }
        return lang.getWebResource();
    }


    /**
     * {@inheritDoc}
     */
    public List<Language> listLanguages() {
        return Lists.newArrayList(languagesByName.values());
    }

    public void addLanguage(final Language language) throws DuplicateLanguageException {
        for (Language lang : listLanguages()) {
            if (lang.getName().equals(language.getName())) {
                throw new DuplicateLanguageException(
                        "newcode.language.register.duplicate.name", language
                        .getName());
            }
        }

        for (String alias : language.getAliases()) {
            if (isLanguageRegistered(alias)) {
                throw new DuplicateLanguageException(
                        "newcode.language.register.duplicate.alias", alias);
            }
        }

        for (String alias : language.getAliases()) {
            languages.put(alias, language);
        }
        languagesByName.put(language.getName(), language);
    }

    /**
     * {@inheritDoc}
     */
    public void unregisterLanguage(final String name) {
        for (Iterator<Map.Entry<String, Language>> i = languages.entrySet()
                .iterator(); i.hasNext(); ) {
            Map.Entry<String, Language> entry = i.next();
            if (entry.getValue().getName().equals(name)
                    && !entry.getValue().isBuiltIn()) {
                i.remove();
            }
        }
        languagesByName.remove(name);
    }
}
