package com.atlassian.confluence.ext.code.languages;

import java.io.Reader;

/**
 * Defines an interface for a component that can install new Languages when an administrator uploads a new JavaScript
 * SyntaxHighlighter brush file to Confluence.
 * <p>
 * The current implementation of this component relies on the plugin framework to register and persist the JavaScript
 * contents provided by the administrator.
 */
public interface RegisteredLanguageInstaller {
    /**
     * Install the specified JavaScript contents as a new custom SyntaxHighlighter brush for the code macro.
     *
     * @param languageScript The JavaScript contents.
     * @param friendlyName   The friendly name entered by the Administrator for this language.
     * @throws InvalidLanguageException   The contents supplied by the Administrator was not a valid custom SyntaxHighlighter brush.
     * @throws DuplicateLanguageException The custom SyntaxHighlighter brush tried to register itself as a language that
     *                                    is already handled by a different brush that is already installed.
     */
    void installLanguage(Reader languageScript, String friendlyName) throws InvalidLanguageException, DuplicateLanguageException;
}
