package com.atlassian.confluence.ext.code.languages.impl;

import com.atlassian.confluence.ext.code.languages.InvalidLanguageException;
import com.atlassian.confluence.ext.code.languages.Language;
import com.atlassian.confluence.ext.code.languages.LanguageParser;
import org.apache.commons.io.IOUtils;
import org.mozilla.javascript.ConsString;
import org.mozilla.javascript.Context;
import org.mozilla.javascript.NativeArray;
import org.mozilla.javascript.RhinoException;
import org.mozilla.javascript.Scriptable;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Uses the server-side Rhino JavaScript engine to parse syntax highlighter configuration files for the code macro,
 * ensuring that they are valid and determining which languages/aliases the file will respond to.
 */
public class RhinoLanguageParser implements LanguageParser {
    private static final String PARSER_VALIDATION_FUNCTION = "SyntaxHighlighter.readBrushes();";
    private final String parserScript;

    public RhinoLanguageParser() {
        InputStream parserStream = null;
        try {
            parserStream = getClass().getClassLoader().getResourceAsStream("languageParser.js");
            parserScript = IOUtils.toString(parserStream);
        } catch (IOException e) {
            // If we can't load the parser script, we should fail visibly, since other parts of the code macro plugin
            // won't function correctly without the LanguageParser in operation.
            throw new RuntimeException("Failed to extract language parser script: " + e.getMessage(), e);
        } finally {
            if (parserStream != null)
                IOUtils.closeQuietly(parserStream);
        }
    }

    /**
     * Parses the language file and returns a new {@link Language} instance.
     *
     * @param languageScript The reader to retrieve the language script
     * @param isBuiltin      Whether this is a built-in script
     * @return The new language
     * @throws com.atlassian.confluence.ext.code.languages.InvalidLanguageException In case the language specification was invalid
     */
    private Language parseLanguage(final Reader languageScript, final boolean isBuiltin, String friendlyName)
            throws InvalidLanguageException {
        StringWriter scriptWriter = new StringWriter();
        try {
            IOUtils.copy(languageScript, scriptWriter);
        } catch (IOException e) {
            throw new InvalidLanguageException(
                    "newcode.language.parse.read.failed", e);
        }

        String scriptString = scriptWriter.toString();

        String name;
        Collection<String> aliases;

        Context cx = Context.enter();
        try {
            Scriptable scope = cx.initStandardObjects();

            String script = parserScript + "\n" + scriptString + "\n"
                    + PARSER_VALIDATION_FUNCTION;

            cx.evaluateString(scope, script, "ParserScript", 0, null);

            Object nameObj = scope.get("brushName", scope);
            if (!(nameObj instanceof ConsString)) {
                throw new InvalidLanguageException(
                        "newcode.language.parse.no.brush.name");
            }
            name = nameObj.toString();
            Object brushAliasesObj = scope.get("brushAliases", scope);
            if (!(brushAliasesObj instanceof NativeArray)) {
                throw new InvalidLanguageException(
                        "newcode.language.parse.no.brush.name");
            }
            NativeArray array = (NativeArray) brushAliasesObj;

            aliases = new ArrayList<String>((int) array.getLength());
            for (int i = 0; i < array.getLength(); i++) {
                Object aliasObj = array.get(i, scope);
                if (!(aliasObj instanceof String)) {
                    throw new InvalidLanguageException(
                            "newcode.language.parse.invalid.alias.type");
                }
                aliases.add((String) aliasObj);
            }
        } catch (RhinoException re) {
            // Wrap RhinoExceptions in a checked exception so that we can handle them meaningfully.
            throw new InvalidLanguageException(re.getMessage(), re);

        } finally {
            Context.exit();
        }

        return isBuiltin ? new BuiltinLanguage(name, aliases)
                : new RegisteredLanguage(name, aliases, friendlyName);
    }

    public BuiltinLanguage parseBuiltInLanguage(Reader reader) throws InvalidLanguageException {
        return (BuiltinLanguage) parseLanguage(reader, true, "");
    }

    public RegisteredLanguage parseRegisteredLanguage(Reader reader, String friendlyName) throws InvalidLanguageException {
        return (RegisteredLanguage) parseLanguage(reader, false, friendlyName);
    }
}
