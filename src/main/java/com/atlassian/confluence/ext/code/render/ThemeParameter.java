/**
 * Copyright 2009 Jeroen Benckhuijsen
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atlassian.confluence.ext.code.render;

import com.atlassian.confluence.ext.code.config.NewcodeSettings;

import java.util.Map;

import static com.atlassian.confluence.ext.code.render.Parameters.*;

/**
 * Parameter implementation for the theme parameter.
 *
 * @author Jeroen Benckhuijsen
 */
public final class ThemeParameter extends MappedParameter {

    /**
     * Default constructor.
     *
     * @param name
     *            The name used by the macro
     * @param mappedName
     *            The name used by the syntax highlighter
     */
    public ThemeParameter(final String name, final String mappedName) {
        super(name, mappedName);
    }

    /**
     * Determines the theme to use.
     *
     * @param parameters
     *            The map of parameters.
     * @return The theme to use
     */
    @Override
    public String getValue(final Map<String, String> parameters) {
        String theme = (String) parameters.get(PARAM_THEME);
        if (theme == null) {
            theme = NewcodeSettings.DEFAULT_THEME_VALUE;
        }
        return theme;
    }

}
