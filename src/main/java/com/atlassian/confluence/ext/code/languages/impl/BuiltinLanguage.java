/**
 * Copyright 2009 Jeroen Benckhuijsen
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atlassian.confluence.ext.code.languages.impl;

import com.atlassian.confluence.ext.code.languages.Language;

import java.util.Collection;

/**
 * Represent languages which are builtin into this macro.
 *
 * @author jeroen
 *
 */
public class BuiltinLanguage implements Language {

    private String name;
    private String friendlyName;
    private Collection<String> aliases;
    private String webResource;

    /**
     * Default constructor.
     * @param name The name of the language
     * @param aliases The aliases of the language
     */
    BuiltinLanguage(final String name, final Collection<String> aliases) {
        super();
        this.name = name;
        this.aliases = aliases;
    }

    /**
     * {@inheritDoc}
     */
    public String getName() {
        return name;
    }

    /**
     * {@inheritDoc}
     */
    public boolean isBuiltIn() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    public Collection<String> getAliases() {
        return aliases;
    }

    /**
     * @return the webResource
     */
    public String getWebResource() {
        return webResource;
    }

    /**
     * @param webResource the webResource to set
     */
    public void setWebResource(final String webResource) {
        this.webResource = webResource;
    }

    public String getFriendlyName() {
        return friendlyName;
    }

    public void setFriendlyName(String friendlyName) {
        this.friendlyName = friendlyName;
    }
}
