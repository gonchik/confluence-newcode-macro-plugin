/**
 * Copyright 2009 Jeroen Benckhuijsen
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atlassian.confluence.ext.code.config;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Persistent settings for the New code macro.
 *
 * @author Jeroen Benckhuijsen
 *
 */
public final class NewcodeSettings implements Serializable {

    public static final String DEFAULT_THEME_VALUE = "Confluence";
    public static final String DEFAULT_LANGUAGE_VALUE = "Java";

    public static final String DEFAULT_THEME = "defaultTheme";
    public static final String DEFAULT_LANGUAGE = "defaultLanguage";

    /**
     * The serial version UID.
     */
    private static final long serialVersionUID = 1L;

    private String defaultTheme;

    private String defaultLanguage;

    /**
     * Default constructor.
     */
    public NewcodeSettings() {
        super();
    }

    /**
     * @return the defaultTheme
     */
    public String getDefaultTheme() {
        return defaultTheme;
    }

    /**
     * @param defaultTheme
     *            the defaultTheme to set
     */
    public void setDefaultTheme(final String defaultTheme) {
        this.defaultTheme = defaultTheme;
    }

    /**
     * @return the defaultLanguage
     */
    public String getDefaultLanguage() {
        return defaultLanguage;
    }

    /**
     * @param defaultLanguage the defaultLanguage to set
     */
    public void setDefaultLanguage(String defaultLanguage) {
        this.defaultLanguage = defaultLanguage;
    }

    /**
     * Map from the object representation to the persistence map.
     * @return The persistence map
     */
    public Map<String, Object> settingsToMap() {
        Map<String, Object> map = new HashMap<String, Object>();

        map.put(DEFAULT_THEME, this.getDefaultTheme());
        map.put(DEFAULT_LANGUAGE, this.getDefaultLanguage());

        return map;
    }

    /**
     * Map from the persistence format (a java.util.Map) to the object representation.
     * @param map The persistence map
     */
    public void mapToSettings(final Map<String, Object> map) {
        this.setDefaultTheme((String) map.get(DEFAULT_THEME));
        this.setDefaultLanguage((String) map.get(DEFAULT_LANGUAGE));
    }

}
