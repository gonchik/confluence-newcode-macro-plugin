/**
 * Copyright 2009 Jeroen Benckhuijsen
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atlassian.confluence.ext.code.descriptor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Facade for changing the Confluence descriptors. Delegates all calls to the
 * actual strategy compatible with the current version of Confluence.
 *
 * @author Jeroen Benckhuijsen
 */
public final class DescriptorFacadeImpl implements DescriptorFacade {

    private static final Logger LOG = LoggerFactory.getLogger(DescriptorFacadeImpl.class);

    /*
     * Injected components
     */
    private final ConfluenceStrategy strategy;

    /**
     * Default constructor.
     */
    public DescriptorFacadeImpl(ConfluenceStrategy loadingStrategy) {
        this.strategy = loadingStrategy;
    }

    /**
     * {@inheritDoc}
     */
    public BrushDefinition[] listBuiltinBrushes() {
        LOG.debug("Retrieving declared brushes");
        BrushDefinition[] result = this.strategy.listBuiltinBrushes();

        if (LOG.isDebugEnabled()) {
            StringBuilder builder = new StringBuilder();
            builder.append("[");
            for (BrushDefinition brush : result) {
                builder.append(brush.getLocation()).append(',');
            }
            builder.append("]");
            LOG.debug("Declared brushes retrieved: {}", builder);
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    public ThemeDefinition[] listBuiltinThemes() {
        LOG.debug("Retrieving declared themes");
        ThemeDefinition[] result = this.strategy.listBuiltinThemes();

        if (LOG.isDebugEnabled()) {
            StringBuilder builder = new StringBuilder();
            builder.append("[");
            for (ThemeDefinition tmp : result) {
                builder.append(tmp.getLocation()).append(',');
            }
            builder.append("]");
            LOG.debug("Declared brushes themes: {}", builder);
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    public List<String> listLocalization() {
        LOG.debug("Retrieving declared localization");
        List<String> result = this.strategy.listLocalization();

        LOG.debug("Declared localizations: {}", result);
        return result;
    }
}
