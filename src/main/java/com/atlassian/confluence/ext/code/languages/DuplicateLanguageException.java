/**
 * Copyright 2009 Jeroen Benckhuijsen
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atlassian.confluence.ext.code.languages;

/**
 * Exception which is thrown by the {@link LanguageRegistry} in case a language
 * is registered with a duplicate name or a duplicate alias.
 *
 * @author Jeroen Benckhuijsen
 *
 */
public final class DuplicateLanguageException extends Exception {

    /**
     * Serial version UID.
     */
    private static final long serialVersionUID = 1L;

    private String languageNameInError;

    /**
     * Default constructor.
     */
    public DuplicateLanguageException(final String msg,
                                      final String languageNameInError) {
        super(msg);
        this.languageNameInError = languageNameInError;
    }

    /**
     * @return The already-registered language name that caused this exception to be thrown.
     */
    public String getLanguageNameInError() {
        return languageNameInError;
    }
}