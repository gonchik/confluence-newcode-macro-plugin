/**
 * Copyright 2009 Jeroen Benckhuijsen
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atlassian.confluence.ext.code.languages;

import java.util.Collection;

/**
 * Interface for the registration of supported languages.
 *
 * @author Jeroen Benckhuijsen
 *
 */
public interface Language {

    String getFriendlyName();

    /**
     * @return The name of the language
     */
    String getName();

    /**
     * @return The aliases for the language
     */
    Collection<String> getAliases();

    /**
     * @return whether the language is part of the macro or external
     */
    boolean isBuiltIn();

    /**
     * @return the id of the web resource to include for this language
     */
    String getWebResource();
}
