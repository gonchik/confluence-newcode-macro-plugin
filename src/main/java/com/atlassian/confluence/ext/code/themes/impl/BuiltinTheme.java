/**
 * Copyright 2009 Jeroen Benckhuijsen
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atlassian.confluence.ext.code.themes.impl;

import java.util.Map;

import com.atlassian.confluence.ext.code.themes.Theme;

/**
 * Represent themes which are builtin into this macro.
 *
 * @author jeroen
 *
 */
public final class BuiltinTheme implements Theme {
    private String name;
    private String styleSheetUrl;
    private String webResource;
    private Map<String, String> defaultLayout;

    /**
     * Default constructor.
     * @param name The name of the theme
     * @param styleSheetUrl The path of the stylesheet
     * @param defaultLayout The default layout of the theme
     */
    public BuiltinTheme(final String name, final String styleSheetUrl, final Map<String, String> defaultLayout) {
        super();
        this.name = name;
        this.styleSheetUrl = styleSheetUrl;
        this.defaultLayout = defaultLayout;
    }

    /**
     * {@inheritDoc}
     */
    public String getName() {
        return name;
    }

    /**
     * {@inheritDoc}
     */
    public String getStyleSheetUrl() {
        return styleSheetUrl;
    }

    /**
     * {@inheritDoc}
     */
    public boolean isBuiltIn() {
        return true;
    }

    /**
     * @return the webResource
     */
    public String getWebResource() {
        return webResource;
    }

    /**
     * @param webResource the webResource to set
     */
    public void setWebResource(final String webResource) {
        this.webResource = webResource;
    }

    /**
     * {@inheritDoc}
     */
    public Map<String, String> getDefaultLayout() {
        return defaultLayout;
    }

}
