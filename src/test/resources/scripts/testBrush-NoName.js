/**
 * A Test SyntaxHighligher custom brush that forgets to declare itself against the SyntaxHighlighter.brushes collection.
**/

function Brush()
{

}
Brush.prototype = new SyntaxHighlighter.Highlighter();
Brush.aliases = ["test1", "test2"];

