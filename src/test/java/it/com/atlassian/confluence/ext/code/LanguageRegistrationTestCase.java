package it.com.atlassian.confluence.ext.code;

import com.atlassian.confluence.plugin.functest.AbstractConfluencePluginWebTestCase;
import com.atlassian.confluence.plugin.functest.helper.PageHelper;
import com.atlassian.confluence.plugin.functest.helper.UserHelper;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.Writer;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Vector;

/**
 * Integration tests added for CONF-1093.
 */
public class LanguageRegistrationTestCase extends AbstractConfluencePluginWebTestCase
{
    private HttpClient httpClient;
    private static final String CUSTOM_LANGUAGE_FILE = "scripts/shBrushBat.js";
    private static final String CUSTOM_LANGUAGE_FRIENDLY_NAME = "Batch Script";
    private static final String CUSTOM_LANGUAGE_NAME = "Cmd";
    private static final String TEST_ADMIN_USER = "confadmin";

    /**
     * Test case setup. Use the stored site-export.zip to setup a testing
     * environment inside Confluence.
     *
     * @throws Exception in case of setup failure.
     */
    @Override
    protected void setUp() throws Exception
    {
        super.setUp();

        this.getConfluenceWebTester().setCurrentUserName("admin");
        this.getConfluenceWebTester().setCurrentPassword("admin");

        disableWebSudo();

        httpClient = HttpClientBuilder.create()
                .build();
    }

    /**
     * Languages are defined as JavaScript files, which could allow an uploader to XSS any other user.
     * Therefore, only system administrators should be permitted to upload new language files.
     */
    public void testThatOnlySysAdminCanUploadLanguage() throws Exception
    {
        UserHelper uh = createUser(TEST_ADMIN_USER, "password");
        try
        {
            try
            {
                grantConfluenceAdministratorPermissionToUser(TEST_ADMIN_USER);
                List<ActionMessage> result = uploadLanguage(convertResourceToTempFile(CUSTOM_LANGUAGE_FILE), CUSTOM_LANGUAGE_FRIENDLY_NAME, TEST_ADMIN_USER, "password");
                assertActionError(result, "Only System Administrators may add new code macro languages");

            }
            finally
            {
                // HACK: Logout to clear any active session from a different login name.
                logoutHttpClient();
            }
        } finally
        {
            uh.delete();
        }
    }

    /**
     * No point showing the upload link to a user that will get a permission denied error if they
     * try to use it.
     */
    public void testThatOnlySysAdminCanSeeUploadLink() throws Exception
    {
        // Create a user who has Confluence admin but not sysadmin.
        UserHelper uh = createUser(TEST_ADMIN_USER, "password");
        try
        {
            grantConfluenceAdministratorPermissionToUser(uh.getName());

            logout();
            login(TEST_ADMIN_USER, "password");
            gotoPage("/admin/plugins/newcode/configure.action");
            assertLinkNotPresent("add-language");
        }
        finally
        {
            uh.delete();
        }
    }

    /**
     * Tests that the code macro doesn't blow up if it specifies a language value that no longer
     * exists (eg. if a languagre is un-installed but is still referenced by macros).
     */
    public void testMacroRendersDefaultLanguageWhenCustomLanguageIsUninstalled() throws Exception
    {
        assertActionSuccess(configureDefaultLanguage(), "Successfully saved the configuration");
        assertActionSuccess(uploadLanguage(convertResourceToTempFile(CUSTOM_LANGUAGE_FILE), CUSTOM_LANGUAGE_FRIENDLY_NAME), "Successfully added the language");

        // Create a page with the code macro rendering the newly-uploaded language.
        PageHelper pageHelper = getPageHelper();
        pageHelper.setSpaceKey("RELTST");
        pageHelper.setTitle("newcode rendering custom language");
        pageHelper.setContent(generateNewcodeMacroContent("Cmd", "REM this is a comment"));
        assertTrue(pageHelper.create());

        uninstallLanguage(CUSTOM_LANGUAGE_NAME);

        gotoPage(this.getConfluenceWebTester().getBaseUrl() + "/pages/viewpage.action?pageId=" + pageHelper.getId());

        assertTrue(getPageSource().contains("brush: java;"));
    }

    /**
     * Tests that the Admin page behaves OK if the currently-specified Default language is un-installed.
     */
    public void testAdminPageIsOkWhenDefaultLanguageUninstalled() throws Exception
    {
        try
        {
            // Upload a new language. Set it as the default, and then un-install it.
            assertActionSuccess(uploadLanguage(convertResourceToTempFile(CUSTOM_LANGUAGE_FILE), CUSTOM_LANGUAGE_FRIENDLY_NAME), "Successfully added the language");
            assertActionSuccess(configureDefaultLanguage(CUSTOM_LANGUAGE_FRIENDLY_NAME), "Successfully saved the configuration");
            assertEquals(CUSTOM_LANGUAGE_FRIENDLY_NAME, getElementTextByXPath("//select[@id='defaultLanguageName']"));
            uninstallLanguage(CUSTOM_LANGUAGE_NAME);

            // Go back to the Admin page - default language should have been reset back to "None".
            gotoPageWithEscalatedPrivileges("/admin/plugins/newcode/configure.action");
            assertTitleEquals("Code Macro Administration - Confluence");
            assertEquals("Java", getElementTextByXPath("//select[@id='defaultLanguageName']"));
        }
        finally
        {
            uninstallLanguage(CUSTOM_LANGUAGE_NAME);
        }
    }

    /**
     * Tests that the code macro actually renders custom language brushes.
     */
    public void testMacroRendersWithCustomLanguage() throws Exception
    {
        try
        {
            assertActionSuccess(configureDefaultLanguage(), "Successfully saved the configuration");
            assertActionSuccess(uploadLanguage(convertResourceToTempFile(CUSTOM_LANGUAGE_FILE), CUSTOM_LANGUAGE_FRIENDLY_NAME), "Successfully added the language");

            // Create a page with the code macro rendering the newly-uploaded language.
            PageHelper pageHelper = getPageHelper();
            pageHelper.setSpaceKey("RELTST");
            pageHelper.setTitle("newcode rendering custom language");
            pageHelper.setContent(generateNewcodeMacroContent("Cmd", "REM this is a comment"));
            assertTrue(pageHelper.create());

            gotoPage(this.getConfluenceWebTester().getBaseUrl() + "/pages/viewpage.action?pageId=" + pageHelper.getId());

            String s = getPageSource();
            assertTrue(s.contains("brush: cmd;"));
        }
        finally
        {
            uninstallLanguage(CUSTOM_LANGUAGE_NAME);
        }
    }

    /**
     * Tests the happy path of a sysadmin uploading a valid language file.
     */
    public void testUploadNewLanguage() throws Exception
    {
        try
        {
            assertActionSuccess(uploadLanguage(convertResourceToTempFile(CUSTOM_LANGUAGE_FILE), CUSTOM_LANGUAGE_FRIENDLY_NAME), "Successfully added the language");
            assertRegisteredLanguagePresent("Cmd", CUSTOM_LANGUAGE_FRIENDLY_NAME);
        }
        finally
        {
            uninstallLanguage(CUSTOM_LANGUAGE_NAME);
        }
    }

    private String generateNewcodeMacroContent(String language, String content)
    {
        StringBuilder s = new StringBuilder();
        s.append("{code:language=").append(language).append("}").append(content).append("{code}");
        return s.toString();
    }

    private UserHelper createUser(String username, String password)
    {
        UserHelper uh = getUserHelper(username);
        uh.setPassword(password);
        uh.setFullName("Test User");
        uh.setEmailAddress("noreply@atlassian.com");

        if (uh.read())
        {
            assertTrue("Could not update user " + username, uh.update());
        } else
        {
            assertTrue("Could not create user " + username, uh.create());
        }
        return uh;
    }

    private void grantConfluenceAdministratorPermissionToUser(String username)
    {
        gotoPage("/admin/permissions/editglobalpermissions.action");
        setWorkingForm("editglobalperms");
        setTextField("usersToAdd", username);
        clickElementByXPath("//input[@name='usersToAddButton']");

        // Page reload happens here
        setWorkingForm("editglobalperms");
        clickElementByXPath("//table[@id='uPermissionsTable']//td/span[text()='("+ username +")']/../../td//input[contains(@name, 'administrateconfluence')]");
        clickElementByXPath("//input[@name='confirm']");
    }

    private void assertActionError(List<ActionMessage> result, String errorMessage)
    {
        assertEquals("Error message was missing from action result", 1, result.size());
        assertTrue("The action succeeded unexpectedly: " + result.get(0).message, result.get(0).isError);
        assertEquals(errorMessage, result.get(0).message);
    }

    private void assertActionSuccess(List<ActionMessage> result, String expectedMessage)
    {
        assertEquals("Success message was missing from upload result", 1, result.size());
        assertFalse("An error occurred while uploading the language: " + result.get(0).message, result.get(0).isError);
        assertEquals("Success message was incorrect", expectedMessage, result.get(0).message.trim());
    }

    private void assertRegisteredLanguagePresent(String name, String friendlyName) throws JSONException, IOException
    {
        // Hit up the getlanguages.action endpoint and make sure it reports the new language is installed.
        HttpGet getLanguages = new HttpGet(this.getConfluenceWebTester().getBaseUrl() + "/plugins/newcode/getlanguages.action");
        getLanguages.addHeader(getAuthHeader("admin", "admin"));

        HttpResponse response = httpClient.execute(getLanguages);
        assertEquals("Expected 200 OK", 200, response.getStatusLine().getStatusCode());

        JSONArray installedLanguages = new JSONArray(IOUtils.toString(response.getEntity().getContent()));
        boolean found = true;
        for (int i = 0; i < installedLanguages.length(); i++)
        {
            JSONObject language = installedLanguages.getJSONObject(i);
            if (language.getString("name").equals(name))
            {
                found = true;
                assertEquals(language.getString("friendlyName"), friendlyName);
                assertFalse(language.getBoolean("builtIn"));
            }
            //A Language: {"webResource":"com.atlassian.confluence.ext.code.custom.Cmd.7882192846065371811:custom-code-syntax-resources","name":"Cmd","friendlyName":"Batch Script","builtIn":false,"aliases":{}}
        }
        assertTrue("Batch Script custom lanuage not found in the list of custom languages found", found);
        EntityUtils.consume(response.getEntity());
    }

    private class ActionMessage
    {
        public ActionMessage(boolean isError, String message)
        {
            this.isError = isError;
            this.message = message;
        }

        public boolean isError;
        public String message;
    }

    private void logoutHttpClient() throws IOException
    {
        HttpGet get = new HttpGet(this.getConfluenceWebTester().getBaseUrl() + "/logout.action");
        HttpResponse response = httpClient.execute(get);
        assertEquals("Expected 200 OK", 200, response.getStatusLine().getStatusCode());
        EntityUtils.consume(response.getEntity());
    }

    private void uninstallLanguage(String languageName) throws IOException
    {
        HttpGet get = new HttpGet(this.getConfluenceWebTester().getBaseUrl() + "/admin/plugins/newcode/removelanguage.action?languageName=" + URLEncoder.encode(languageName, "utf-8"));
        get.addHeader(getAuthHeader("admin", "admin"));
        get.addHeader("X-Atlassian-Token", "no-check");

        HttpResponse response = httpClient.execute(get);
        assertEquals("Expected 200 OK", 200, response.getStatusLine().getStatusCode());

        String body = IOUtils.toString(response.getEntity().getContent());
        Document soup = Jsoup.parse(body);
        Elements actionResult = soup.select("#admin-body-content");
        assertEquals(1, actionResult.size());
        assertEquals("success", actionResult.get(0).text());
        EntityUtils.consume(response.getEntity());
    }

    private Header getAuthHeader(String username, String password)
    {
        String encoded = Base64.encodeBase64String((username + ":" + password).getBytes());
        encoded = encoded.replaceAll("\\r\\n", "");
        return new BasicHeader("Authorization", "Basic " + encoded);
    }

    private List<ActionMessage> uploadLanguage(File languageFile, String friendlyName, String username, String password) throws Exception
    {
        // Upload a new language using an HTTP form post - can't test using the UI because it's JS-based and the
        // integration tests in this project are all JWebUnit based.
        HttpPost post = new HttpPost(this.getConfluenceWebTester().getBaseUrl() + "/admin/plugins/newcode/addlanguage.action");

        FileBody fileBody = new FileBody(languageFile);
        StringBody newLanguageName = new StringBody(friendlyName);

        MultipartEntity entity = new MultipartEntity();
        entity.addPart("file_0", fileBody);
        entity.addPart("newLanguageName", newLanguageName);
        post.setEntity(entity);
        post.setHeader("X-Atlassian-Token", "no-check");
        post.addHeader(getAuthHeader(username, password));

        HttpResponse response = httpClient.execute(post);

        assertEquals("Expected 200 OK", 200, response.getStatusLine().getStatusCode());

        // Parse the returned HTML and look for a success message.
        String pageBody = IOUtils.toString(response.getEntity().getContent());
        EntityUtils.consume(response.getEntity());
        return parsePageBodyForActionMessages(pageBody);
    }

    private List<ActionMessage> parsePageBodyForActionMessages(String pageBody)
    {
        Document successPage = Jsoup.parse(pageBody);

        // Look for any errors
        Elements errors = successPage.select(".aui-message-error li");
        if (!errors.isEmpty())
        {
            List<ActionMessage> errorMessages = new ArrayList<ActionMessage>();
            for (Element error : errors)
            {
                errorMessages.add(new ActionMessage(true, error.text()));
            }
            return errorMessages;
        }

        // Ensure success message
        Elements success = successPage.select(".aui-message.success");
        if (!success.isEmpty())
        {
            return Collections.singletonList(new ActionMessage(false, success.text()));
        }

        return Collections.emptyList();
    }

    private List<ActionMessage> uploadLanguage(File languageFile, String friendlyName) throws Exception
    {
        return uploadLanguage(languageFile, friendlyName, "admin", "admin");
    }

    private File convertResourceToTempFile(String resourceName) throws IOException
    {
        // Extract the resource
        InputStream stream = this.getClass().getClassLoader().getResourceAsStream(resourceName);
        try
        {
            // Copy to a file.
            Reader r = new InputStreamReader(stream);

            // Create the file to store the extracted resource
            File f = File.createTempFile("brush", ".js");
            Writer w = null;
            try
            {
                w = new FileWriter(f);
                IOUtils.copy(r, w);
                return f;
            }
            finally
            {
                if (w != null)
                    w.close();
            }
        }
        finally
        {
            IOUtils.closeQuietly(stream);
        }
    }

    private void disableWebSudo() throws Exception {
        final String token = getConfluenceWebTester().loginToXmlRpcService("admin", "admin");
        final Vector<Object> parameters = new Vector<>();
        parameters.add(token);
        parameters.add(false);
        getConfluenceWebTester().getXmlRpcClient().execute("functest.enableWebSudo", parameters);
        getConfluenceWebTester().logoutFromXmlRpcService(token);
    }

    private List<ActionMessage> configureDefaultLanguage(String languageFriendlyName)
    {
        gotoPageWithEscalatedPrivileges("/admin/plugins/newcode/configure.action");
        assertTitleEquals("Code Macro Administration - Confluence");
        setWorkingForm("configure");
        selectOption("defaultLanguageName", languageFriendlyName);
        submit();

        return parsePageBodyForActionMessages(getPageSource());
    }

    private List<ActionMessage> configureDefaultLanguage()
    {
        return configureDefaultLanguage("Java");
    }
}
