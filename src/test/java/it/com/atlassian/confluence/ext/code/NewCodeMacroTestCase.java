/**   Copyright 2009 Jeroen Benckhuijsen
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 *
 */
package it.com.atlassian.confluence.ext.code;

import com.atlassian.confluence.ext.code.NewCodeMacro;
import com.atlassian.confluence.plugin.functest.AbstractConfluencePluginWebTestCase;
import com.atlassian.confluence.plugin.functest.helper.PageHelper;
import com.atlassian.confluence.util.GeneralUtil;

/**
 * @author Jeroen Benckhuijsen
 * 
 *         Integration test case for the {@link NewCodeMacro} plugin.
 *
 */
public final class NewCodeMacroTestCase extends AbstractConfluencePluginWebTestCase {

    private final String spacePrefix = "REL";
    private final String macro = "code";
    private static boolean pdlEnabled = Long.parseLong(GeneralUtil.getBuildNumber()) >= 4000;

    /**
     * Constructor.
     */
    public NewCodeMacroTestCase() {
        super();
    }

    /**
     * Test case setup. Use the stored site-export.zip to setup a testing
     * environment inside Confluence.
     * @throws Exception in case of setup failure.
     */
    @Override
    protected void setUp() throws Exception {
        super.setUp();

        this.getConfluenceWebTester().setCurrentUserName("admin");
        this.getConfluenceWebTester().setCurrentPassword("admin");
    }

    /**
     * Returns the correct url of a page based on the key used by the plugin in
     * the rendered content. This is used to ensure release testing succeeds,
     * where the key "code" is used by default instead of "newcode" for normal
     * integration testing.
     * 
     * @param url
     *            The pseudo-url of the page
     * @return The actual page url with the correct space specified.
     */
    private String getPageUrl(final String url) {
        return url.replaceAll("SPACE", this.spacePrefix + "TST");
    }

    /**
     * Test whether Java code is rendered.
     */
    public void testJavaCode() {
        gotoPage(getPageUrl("/display/SPACE/LanguageTest"));
        String response = this.getTester().getPageSource();

        if(pdlEnabled)
        {
            assertTrue("Page does not contain a code panel", response
                    .contains("<div class=\"code panel pdl"));
        }
        else
        {
            assertTrue("Page does not contain a code panel", response
                    .contains("<div class=\"code panel"));
        }
        assertTrue("Java code has not been formatted:" + response, response
                .contains("brush: java"));

        // <div class="code panel" style="border-width: 1px;"><div
        // class="codeContent panelContent">
        // <pre class="; brush: java">public class TestClass &#123;
        // public static void main (String&#92;&#91;&#92;&#93; args) &#123;
        // System.out.println("Hello World&#92;&#33;");
        // &#125;
        // &#125;
        // </pre>
        // </div></div>
    }

    /**
     * Test whether a title section is shown.
     */
    public void testTitle() {
        gotoPage(getPageUrl("display/SPACE/TitleTest"));
        String response = this.getTester().getPageSource();

        if (pdlEnabled)
        {
            assertTrue(
                    "Page does not contain the title",
                    response.contains("<div class=\"codeHeader panelHeader pdl\" style=\"border-bottom-width: 1px;\">"
                            + "<b>TestTitle</b>"
                            + "</div>"));
        }
        else
        {
            assertTrue(
                    "Page does not contain the title",
                    response.contains("<div class=\"codeHeader panelHeader\" style=\"border-bottom-width: 1px;\">"
                            + "<b>TestTitle</b>"
                            + "</div>"));
        }

        // <!-- wiki content -->
        // <div class="code panel" style="border-width: 1px;"><div
        // class="codeHeader panelHeader"
        // style="border-bottom-width: 1px;"><b>TestTitle</b></div><div
        // class="codeContent panelContent">
        // <pre class="; brush: java">
        // public class Test &#123;
        // public static void main (String&#91;&#93; args) &#123;
        // System.out.println("Hello World&#33;");
        // &#125;
        // &#125;
        // </pre>
        // </div></div>
        // </div>
    }

    /**
     * Test whether collapsing is handled correctly.
     */
    public void testCollapse() {
        gotoPage(getPageUrl("display/SPACE/CollapseTest"));
        String response = this.getTester().getPageSource();

        assertTrue("Page is not collapsed", response.contains("collapse: true"));

        // <div class="code panel" style="border-width: 1px;"><div
        // class="codeContent panelContent">
        // <pre class="toolbar: true; brush: java; collapse: true">
        // public class Test &#123;
        // public static void main (String&#91;&#93; args) &#123;
        // System.out.println("Hello World&#33;");
        // &#125;
        // &#125;
        // </pre>
        // </div></div>
    }

    /**
     * Test whether linenumbers is handled correctly.
     */
    public void testLinenumbers() {
        gotoPage(getPageUrl("display/SPACE/LinenumbersTest"));
        String response = this.getTester().getPageSource();

        assertTrue("Linenumbers still visible", response
                .contains("gutter: false"));

        // <div class="code panel" style="border-width: 1px;"><div
        // class="codeContent panelContent">
        // <pre class="; gutter: false; brush: java">
        // public class Test &#123;
        // public static void main (String&#91;&#93; args) &#123;
        // System.out.println("Hello World&#33;");
        // &#125;
        // &#125;
        // </pre>
        // </div></div>
        // </div>

    }

    /**
     * Test whether first line specification is handled correctly.
     */
    public void testFirstline() {
        gotoPage(getPageUrl("display/SPACE/FirstlineTest"));
        String response = this.getTester().getPageSource();

        assertTrue("Incorrect firstline encountered", response
                .contains("first-line: 10"));

        // <div class="code panel" style="border-width: 1px;"><div
        // class="codeContent panelContent">
        // <pre class="; brush: java; first-line: 10">
        // public class Test &#123;
        // public static void main (String&#91;&#93; args) &#123;
        // System.out.println("Hello World&#33;");
        // &#125;
        // &#125;
        // </pre>
        // </div></div>
        // </div>

    }

    /**
     * Test exporting of the page.
     */
    public void testExport() {
        // http://localhost:1990/confluence/pages/doexportpage.action?pageId=819210&type=TYPE_PDF
        // TODO
    }

    public void testNewcodeMacroAcceptLanguageParameter()
    {
    	configureDefaultLanguage();
    	PageHelper pageHelper = getPageHelper();
    	pageHelper.setSpaceKey(this.spacePrefix + "TST");
    	pageHelper.setTitle("newcode using language parameter");
    	pageHelper.setContent(generateNewcodeMacroContent());
    	assertTrue(pageHelper.create());
    	gotoPage("/pages/viewpage.action?pageId=" + pageHelper.findBySpaceKeyAndTitle());
    	String response = this.getTester().getPageSource();
    	// if 'language' is not recognized, it will use the default language (Java), and response will contain 'brush: Java'
        assertTrue("{newcode} does not use language parameter", response
                 .contains("brush: sql"));
    	
    }
    
    private String generateNewcodeMacroContent()
    {
    	StringBuilder s = new StringBuilder();
    	s.append("{");
    	s.append(this.macro);
    	s.append(":");
    	s.append("language=sql");
    	s.append("}");
    	s.append("SELECT * FROM users;");
    	s.append("{");
    	s.append(this.macro);
    	s.append("}");
    	return s.toString();
    }
    
    private void configureDefaultLanguage()
    {
    	gotoPageWithEscalatedPrivileges("/admin/plugins/newcode/configure.action");
    	assertTitleEquals("Code Macro Administration - Confluence");
    	setWorkingForm("configure");
    	selectOption("defaultLanguageName", "Java");
    	submit();
    }
    
}
