/**   Copyright 2009 Jeroen Benckhuijsen
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package com.atlassian.confluence.ext.code.languages.impl;

import com.atlassian.confluence.ext.code.languages.DuplicateLanguageException;
import com.atlassian.confluence.ext.code.languages.InvalidLanguageException;
import com.atlassian.confluence.ext.code.languages.Language;
import com.atlassian.confluence.ext.code.languages.UnknownLanguageException;
import com.atlassian.confluence.ext.code.languages.installers.BuiltInLanguageInstaller;
import junit.framework.TestCase;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Unit test-case for {@link LanguageRegistryImpl}
 * 
 * @author Jeroen Benckhuijsen
 * 
 */
public final class LanguageRegistryImplTestCase extends TestCase {

    private LanguageDescriptorFacadeMock facadeMock;
    
    /**
     * {@inheritDoc} 
     */
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        facadeMock = new LanguageDescriptorFacadeMock();
    }

    /**
     * Tests behaviour for null aliases.
     * 
     * @throws IOException In of test errors
     * @throws DuplicateLanguageException
     *             In case of invalid built-in languages
     * @throws InvalidLanguageException
     *             In case of duplicate built-in languages
     */
    public void testUnknownAlias() throws IOException, InvalidLanguageException, DuplicateLanguageException {
        LanguageRegistryImpl registry = new LanguageRegistryImpl();

        String result = null;
        try {
            result = registry.getWebResourceForLanguage("UNKNOWN ALIAS");
            fail();
        } catch (UnknownLanguageException e) {
            // expected
            assertNull(result);
        }
    }

    /**
     * Tests behaviour for duplicate aliases.
     * 
     * @throws IOException In of test errors
     * @throws DuplicateLanguageException
     *             In case of invalid built-in languages
     * @throws InvalidLanguageException
     *             In case of duplicate built-in languages
     */
    public void testDuplicateNameAndAlias() throws IOException, InvalidLanguageException, DuplicateLanguageException {
        LanguageRegistryImpl registry = new LanguageRegistryImpl();

        BuiltinLanguage lang1 = new BuiltinLanguage("TEST1", Arrays.asList("Test1", "Test2"));
        BuiltinLanguage lang2 = new BuiltinLanguage("TEST2", Arrays.asList("Test2", "Test3"));
        BuiltinLanguage lang3 = new BuiltinLanguage("TEST1", Arrays.asList("Test3", "Test4"));
        
        registry.addLanguage(lang1);
        
        try {
            registry.addLanguage(lang2);
            fail();
        } catch (DuplicateLanguageException e) {
            // expected
            assertTrue(true);
        }

        try {
            registry.addLanguage(lang3);
            fail();
        } catch (DuplicateLanguageException e) {
            // expected
            assertTrue(true);
        }
    }

    public void testBuiltInLanguageCannotBeUnregistered() throws Exception
    {
        LanguageRegistryImpl registry = new LanguageRegistryImpl();
        mockBuiltinLoading(registry);

        assertTrue(registry.isLanguageRegistered("java"));
        registry.unregisterLanguage("java");
        assertTrue(registry.isLanguageRegistered("java"));
    }

    /**
     * Tests whether unregistration works succesfully.
     * @throws Exception In case of test failures 
     */
    public void testUnregisterLanguage() throws Exception {
        LanguageRegistryImpl registry = new LanguageRegistryImpl();
        mockBuiltinLoading(registry);

        Language lang = new RegisteredLanguage("test", Arrays.asList("test", "test2"), "My Test Language");
        registry.addLanguage(lang);

        assertTrue(registry.isLanguageRegistered("test"));
        assertTrue(registry.isLanguageRegistered("test2"));
        
        registry.unregisterLanguage("test");
        assertFalse(registry.isLanguageRegistered("test"));
        assertFalse(registry.isLanguageRegistered("test2"));
    }

    /**
     * Checks whether for each of the built-in languages the correct script file
     * exists.
     * @throws Exception In case of test failures 
     */
    public void testBuiltinPresent() throws Exception {
        LanguageRegistryImpl registry = new LanguageRegistryImpl();
        mockBuiltinLoading(registry);

        Set<String> aliases = new HashSet<String>();
        for (Language lang : registry.listLanguages()) {
            assertTrue("No aliases for language" + lang.getName(), lang
                    .getAliases().size() > 0);
            for (String alias : aliases) {
                assertFalse("Duplicate alias detected: " + alias
                        + " for language + " + lang.getName(), aliases
                        .contains(alias));

                aliases.add(alias);
            }
        }
    }
    
    /**
     * Test the parsing and registration of the aliases for a language. For this,
     * the ActionScript3 language is used as a test-case.
     * @throws Exception In case of test failures
     */
    public void testAliasParsing() throws Exception {
        LanguageRegistryImpl registry = new LanguageRegistryImpl();
        mockBuiltinLoading(registry);
        
        Collection<Language> languages = registry.listLanguages();
        Language as3 = null;
        for (Language lang : languages) {
            if ("AS3".equals(lang.getName())) {
                as3 = lang;
                break;
            }
        }
        
        assertNotNull(as3);
        Collection<String> aliases = as3.getAliases();
        assertTrue(aliases.contains("actionscript3"));
        assertTrue(aliases.contains("as3"));
        assertTrue(aliases.contains("actionscript"));
        assertTrue(aliases.size() == 3);
    }

    /**
     * Setup mocking of built-in language loading.
     * @param registry The language registry to mock loading for
     * @throws Exception In case of loading errors
     */
    private void mockBuiltinLoading(final LanguageRegistryImpl registry) throws Exception {
        BuiltInLanguageInstaller builtInLanguageInstaller = new BuiltInLanguageInstaller(facadeMock.descriptorFacade, new RhinoLanguageParser(), registry);
        builtInLanguageInstaller.onStart();
    }
}
