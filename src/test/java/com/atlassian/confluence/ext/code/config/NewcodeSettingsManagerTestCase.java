package com.atlassian.confluence.ext.code.config;

import com.atlassian.confluence.ext.code.util.Constants;
import com.atlassian.confluence.setup.settings.SettingsManager;
import junit.framework.TestCase;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.Serializable;
import java.util.HashMap;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class NewcodeSettingsManagerTestCase extends TestCase
{
    private NewcodeSettingsManager settingsManager;

    @Mock private SettingsManager confluenceSettingsManager;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();

        MockitoAnnotations.initMocks(this);
        settingsManager = new NewcodeSettingsManager(confluenceSettingsManager);
    }

    public void testLoadEmptySettings() throws Exception
    {
        HashMap<String, Object> initialSettings = new HashMap<String, Object>();
        when(confluenceSettingsManager.getPluginSettings(Constants.PLUGIN_KEY)).thenReturn(initialSettings);

        NewcodeSettings settings = settingsManager.getCurrentSettings();
        assertEquals(null, settings.getDefaultLanguage());
        assertEquals(null, settings.getDefaultTheme());
    }

    public void testSave() throws Exception
    {
        settingsManager.updateSettings("NEW_THEME", "NEW_LANGUAGE");

        verify(confluenceSettingsManager).updatePluginSettings(eq(Constants.PLUGIN_KEY), Mockito.<Serializable>any());
    }

    public void testLoadExistingSettings() throws Exception
    {
        HashMap<String, Object> initialSettings = new HashMap<String, Object>();
        initialSettings.put(NewcodeSettings.DEFAULT_LANGUAGE, "DEFAULT_LANGUAGE");
        initialSettings.put(NewcodeSettings.DEFAULT_THEME, "DEFAULT_THEME");

        when(confluenceSettingsManager.getPluginSettings(Constants.PLUGIN_KEY)).thenReturn(initialSettings);

        NewcodeSettings settings = settingsManager.getCurrentSettings();
        assertEquals("DEFAULT_LANGUAGE", settings.getDefaultLanguage());
        assertEquals("DEFAULT_THEME", settings.getDefaultTheme());
    }

    @Override
    protected void tearDown() throws Exception
    {
        super.tearDown();
    }
}
