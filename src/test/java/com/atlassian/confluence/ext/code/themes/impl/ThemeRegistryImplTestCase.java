/**   Copyright 2009 Jeroen Benckhuijsen
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package com.atlassian.confluence.ext.code.themes.impl;

import java.io.InputStream;

import junit.framework.TestCase;

import com.atlassian.confluence.ext.code.themes.Theme;
import com.atlassian.confluence.ext.code.themes.UnknownThemeException;

/**
 * Unit test-case for the {@link ThemeRegistryImpl} and the {@link ThemeParser}.
 * 
 * @author Jeroen Benckhuijsen
 * 
 */
public final class ThemeRegistryImplTestCase extends TestCase {
    
    private ThemeDescriptorFacadeMock facadeMock;

    /**
     * {@inheritDoc} 
     */
    @Override
    protected void setUp() throws Exception {
        facadeMock = new ThemeDescriptorFacadeMock();
    }

    /**
     * Tests whether the built-in languages XML can be successfully parsed.
     * @throws Exception In case of test errors
     */
    public void testParsing() throws Exception {
        ThemeRegistryImpl registry = new ThemeRegistryImpl();
        
        mockBuiltinLoading(registry);

        assertTrue(registry.isThemeRegistered("Default"));
    }
    
    /**
     * Tests for the behaviour when trying to access unknown themes.
     * @throws Exception In case of test errors
     */
    public void testUnknownTheme() throws Exception {
        ThemeRegistryImpl registry = new ThemeRegistryImpl();
        
        mockBuiltinLoading(registry);

        assertFalse(registry.isThemeRegistered("UNKNOWN"));
        String result = null;
        try {
            result = registry.getWebResourceForTheme("UNKNOWN");
            fail();
        } catch (UnknownThemeException e) {
            assertNull(result);
        }
    }
    
    /**
     * Checks whether for each of the builtin theme the correct script file
     * exists.
     * @throws Exception In case of test errors
     */
    public void testBuiltinPresent() throws Exception {
        ThemeRegistryImpl registry = new ThemeRegistryImpl();

        mockBuiltinLoading(registry);

        for (Theme theme : registry.listThemes()) {
            assertNotNull(theme.getName());

            if (theme.isBuiltIn()) {
                InputStream is = null;
                try {
                    is = getClass().getClassLoader().getResourceAsStream(
                            theme.getStyleSheetUrl());
                    assertNotNull("No theme file found for theme "
                            + theme.getName(), is);
                } finally {
                    if (is != null) {
                        is.close();
                    }
                }
            }
        }
    }
    
    /**
     * Setup mocking of built-in theme loading.
     * @param registry The theme registry to mock loading for
     * @throws Exception In case of loading errors
     */
    private void mockBuiltinLoading(final ThemeRegistryImpl registry) throws Exception {
        facadeMock.setupMock(registry);

        registry.afterPropertiesSet();
    }

}
