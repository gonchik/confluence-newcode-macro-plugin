/**   Copyright 2009 Jeroen Benckhuijsen
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package com.atlassian.confluence.ext.code.descriptor;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import junit.framework.TestCase;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

import static org.mockito.Mockito.when;

/**
 * Test case for the functionality to interact with Confluence plugin 
 * descriptors.
 * @author Jeroen Benckhuijsen
 *
 */
public final class DescriptorTestCase extends TestCase {

    @Mock private ConfluenceStrategy confluenceStrategy;

    private DescriptorFacade facade;

    /**
     * {@inheritDoc}
     */
    protected void setUp() throws Exception 
    {
        super.setUp();
        MockitoAnnotations.initMocks(this);
        facade = new DescriptorFacadeImpl(confluenceStrategy);
    }

    /**
     * Test listing of the brushes as declared in atlassian-plugin.xml.
     * @throws Exception In case of test failures
     */
    public void testListBrushes() throws Exception
    {
        BrushDefinition[] expected = {new BrushDefinition("location", "webResourceId")};
        when(confluenceStrategy.listBuiltinBrushes()).thenReturn(expected);

        BrushDefinition[] actual = facade.listBuiltinBrushes();

        assertEquals(expected, actual);
    }

    /**
     * Test listing of the themes as declared in atlassian-plugin.xml.
     *
     * @throws Exception In case of test failures
     */
    public void testListThemes() throws Exception
    {
        ThemeDefinition[] expected = {new ThemeDefinition("location", "webResourceId", ImmutableMap.<String, String>of("one", "two"))};
        when(confluenceStrategy.listBuiltinThemes()).thenReturn(expected);

        ThemeDefinition[] actual = facade.listBuiltinThemes();

        assertEquals(expected, actual);
    }

    public void listLocalizations() throws Exception
    {
        List<String> expected = ImmutableList.of("one", "two");
        when(confluenceStrategy.listLocalization()).thenReturn(expected);

        List<String> actual = facade.listLocalization();

        assertEquals(expected, actual);
    }
}
