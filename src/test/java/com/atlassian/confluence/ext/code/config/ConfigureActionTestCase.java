/**   Copyright 2009 Jeroen Benckhuijsen
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package com.atlassian.confluence.ext.code.config;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.*;

import com.atlassian.confluence.ext.code.languages.RegisteredLanguageInstaller;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.opensymphony.xwork.Action;
import junit.framework.TestCase;

import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.atlassian.confluence.ext.code.languages.Language;
import com.atlassian.confluence.ext.code.languages.LanguageRegistry;
import com.atlassian.confluence.ext.code.themes.Theme;
import com.atlassian.confluence.ext.code.themes.ThemeRegistry;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

/**
 * Test case for the ConfigureNewcodeAction XWork class.
 * 
 * @author Jeroen Benckhuijsen
 * 
 */
public final class ConfigureActionTestCase extends TestCase {
    
    private ConfigureNewcodeAction configureAction;

    @Mock
    private NewcodeSettingsManager settingsManager;
    @Mock
    private LanguageRegistry languageRegistry;
    @Mock
    private ThemeRegistry themeRegistry;
    @Mock
    private RegisteredLanguageInstaller registeredLanguageInstaller;
    @Mock
    private PermissionManager permissionManager;
    @Mock
    private I18NBean i18NBean;

    /**
     * {@inheritDoc}
     */
    @Override
    public void setUp() throws Exception
    {
    	MockitoAnnotations.initMocks(this);

        configureAction = new ConfigureNewcodeAction();
        configureAction.setNewcodeSettingsManager(settingsManager);
        configureAction.setThemeRegistry(themeRegistry);
        configureAction.setLanguageInstaller(registeredLanguageInstaller);
        configureAction.setLanguageRegistry(languageRegistry);
        configureAction.setPermissionManager(permissionManager);

        // Mock I18NBean just echoes back the key it was supplied
        configureAction.setI18NBean(i18NBean);
        Answer<String> echoAnswer = new Answer<String>() {
            public String answer(InvocationOnMock invocationOnMock) throws Throwable {
                return (String) invocationOnMock.getArguments()[0];
            }
        };
        when(i18NBean.getText(anyString())).thenAnswer(echoAnswer);
        when(i18NBean.getText(anyString(), Matchers.eq(Collections.EMPTY_LIST))).thenAnswer(echoAnswer);
    }
    
    @Override
    public void tearDown() throws Exception
    {
    	configureAction = null;
    	settingsManager = null;
    	languageRegistry = null;
    	themeRegistry = null;
        registeredLanguageInstaller = null;
    	super.tearDown();
    }

    private NewcodeSettings mockNewcodeSettings(String defaultLanguage, String defaultTheme)
    {
        NewcodeSettings settings = new NewcodeSettings();
        settings.setDefaultLanguage(defaultLanguage);
        settings.setDefaultTheme(defaultTheme);
        return settings;
    }

    /**
     * Test input retrieval for the configure action.
     * 
     * @throws Exception
     *             In case of test failures
     * 
     */
    public void testInput() throws Exception
    {
        Language mockLanguage = mock(Language.class);
        when(mockLanguage.getName()).thenReturn("DEFAULT_LANGUAGE");
        when(settingsManager.getCurrentSettings()).thenReturn(mockNewcodeSettings("DEFAULT_LANGUAGE", "DEFAULT_THEME"));
        when(languageRegistry.getLanguage("DEFAULT_LANGUAGE")).thenReturn(mockLanguage);

        assertEquals(Action.INPUT, configureAction.input());
        assertEquals("DEFAULT_LANGUAGE", configureAction.getDefaultLanguageName());
        assertEquals("DEFAULT_THEME", configureAction.getCurrentDefaultThemeName());
    }

    public void testAddLanguageWithMissingLanguageName() throws Exception
    {
        when(permissionManager.hasPermission(null, Permission.ADMINISTER, PermissionManager.TARGET_SYSTEM)).thenReturn(true);
        assertEquals(Action.INPUT, configureAction.addLanguage());
        assertEquals(1, configureAction.getActionErrors().size());
        assertEquals("newcode.config.language.add.friendlyname.required", configureAction.getActionErrors().iterator().next());
    }

    public void testAddLanguageNoSysadminPermission() throws Exception
    {
        when(permissionManager.hasPermission(null, Permission.ADMINISTER, PermissionManager.TARGET_SYSTEM)).thenReturn(false);
        assertEquals(Action.ERROR, configureAction.addLanguage());
        assertEquals(1, configureAction.getActionErrors().size());
        assertEquals("newcode.config.language.add.sysadmin.required", configureAction.getActionErrors().iterator().next());
    }

    public void testAddLanguageWithNameTooLong() throws Exception
    {
        when(permissionManager.hasPermission(null, Permission.ADMINISTER, PermissionManager.TARGET_SYSTEM)).thenReturn(true);
        configureAction.setNewLanguageName("This is a very long language name which is undoubtedly going to exceed the maximum supported length of the language name field");

        assertEquals(Action.INPUT, configureAction.addLanguage());
        assertEquals(1, configureAction.getActionErrors().size());
        assertEquals("newcode.config.language.add.friendlyname.length", configureAction.getActionErrors().iterator().next());
    }


    /**
     * Test theme retrieval.
     */
    public void testListingThemes() {
        Collection<Theme> themes = new ArrayList<Theme>();

        when(themeRegistry.listThemes()).thenReturn(themes);

        List<String> result = configureAction.getThemes();
        assertNotNull(result);
        assertTrue(result.size() == 0);

        themes.add(new Theme() {

            public String getName() {
                return "TEST_THEME";
            }

            public String getStyleSheetUrl() {
                return null;
            }

            public boolean isBuiltIn() {
                return false;
            }

            public String getWebResource() {
                return null;
            }

            public Map<String, String> getDefaultLayout() {
                return null;
            }

        });
        
        when(themeRegistry.listThemes()).thenReturn(themes);

        result = configureAction.getThemes();
        assertNotNull(result);
        assertTrue(result.size() == 1);
        assertEquals("TEST_THEME", result.get(0));
    }

    /**
     * Test setting of the theme.
     * 
     * @throws Exception
     *             In case of test failures
     */
    public void testSettingTheme() throws Exception {

        configureAction.setDefaultThemeName("TEST_THEME");
        assertEquals(Action.SUCCESS, configureAction.save());
        assertEquals(1, configureAction.getActionMessages().size());

        verify(settingsManager).updateSettings("TEST_THEME", null);
    }

    /**
     * Test theme retrieval.
     */
    public void testListingLanguages() {
        List<Language> languages = new ArrayList<Language>();
        when(languageRegistry.listLanguages()).thenReturn(languages);

        List<Language> result = configureAction.getLanguages();
        assertNotNull(result);
        assertTrue(result.size() == 0);

        Language testLang = new Language()
        {

            public Collection<String> getAliases()
            {
                return null;
            }

            public String getName()
            {
                return "TEST_LANGUAGE";
            }

            public String getFriendlyName()
            {
                return "My Test Language";
            }

            public String getWebResource()
            {
                return null;
            }

            public boolean isBuiltIn()
            {
                return false;
            }

        };
        languages.add(testLang);
        
        when(languageRegistry.listLanguages()).thenReturn(languages);

        result = configureAction.getLanguages();
        assertNotNull(result);
        assertTrue(result.size() == 1);
        assertEquals(testLang, result.get(0));
    }

    /**
     * Test setting of the theme.
     * 
     * @throws Exception
     *             In case of test failures
     */
    public void testSettingLanguage() throws Exception 
    {
        configureAction.setDefaultLanguageName("TEST_LANGUAGE");
        assertEquals(Action.SUCCESS, configureAction.save());

        assertEquals(1, configureAction.getActionMessages().size());
        verify(settingsManager).updateSettings(null, "TEST_LANGUAGE");
    }
}
