package com.atlassian.confluence.ext.code.descriptor;

import com.atlassian.confluence.ext.code.util.Constants;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.impl.StaticPlugin;
import com.atlassian.plugin.webresource.WebResourceModuleDescriptor;
import junit.framework.TestCase;
import org.apache.commons.io.IOUtils;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.InputStream;
import java.util.Iterator;
import java.util.List;

import static org.mockito.Mockito.when;

public class ConfluenceStrategyTestCase extends TestCase
{
    private static final int EXPECTED_BUILTIN_BRUSHES_COUNT = 25;
    private static final int EXPECTED_BUILTIN_THEMES_COUNT = 8;
    private static final int EXPECTED_LOCALIZATIONS_COUNT = 3;

    @Mock
    private PluginAccessor pluginAccessor;

    private ConfluenceStrategy strategy;

    @Override
    protected void setUp() throws Exception
    {
        MockitoAnnotations.initMocks(this);

        Plugin plugin = createMockPlugin();
        when(pluginAccessor.getPlugin(Constants.PLUGIN_KEY)).thenReturn(plugin);

        strategy = new ConfluenceStrategyImpl(pluginAccessor);
    }

    public void testListBuiltInBrushes() throws Exception
    {
        BrushDefinition[] brushes = strategy.listBuiltinBrushes();
        assertNotNull(brushes);
        assertEquals(EXPECTED_BUILTIN_BRUSHES_COUNT, brushes.length);

        // Spot check
        boolean found = false;
        for (BrushDefinition brush : brushes)
        {
            if (brush.getLocation().equals("sh/scripts/shBrushCSharp.js")
                    && brush.getWebResourceId().equals(Constants.PLUGIN_KEY + ":" + "syntaxhighlighter-brushes"))
            {
                found = true;
                break;
            }
        }
        assertTrue(found);
    }

    public void testListBuiltInThemes() throws Exception
    {
        ThemeDefinition[] themes = strategy.listBuiltinThemes();
        assertNotNull(themes);
        assertEquals(EXPECTED_BUILTIN_THEMES_COUNT, themes.length);

        // Spot check
        boolean found = false;
        for (ThemeDefinition theme : themes)
        {
            if (theme.getLocation().equals("sh/styles/shThemeDjango.css")
                    && theme.getWebResourceId().equals(Constants.PLUGIN_KEY + ":" + "sh-theme-django"))
            {
                found = true;
                break;
            }
        }
        assertTrue(found);
    }

    /**
     * Create a mock plugin to test loading of brushes and themes.
     * @return The plugin
     * @throws DocumentException In case of an invalid mock data file
     */
    private Plugin createMockPlugin() throws DocumentException
    {
        Element element = loadModuleDescriptor("atlassian-plugin.xml");
        Plugin plugin = new StaticPlugin() {

            /**
             * {@inheritDoc}
             */
            @Override
            public String getKey() {
                return Constants.PLUGIN_KEY;
            }

        };

        for (Iterator i = element.elementIterator(); i.hasNext();) {
            Element child = (Element) i.next();
            if (child.getName().equals("web-resource"))
            {
                final WebResourceModuleDescriptor descriptor = new WebResourceModuleDescriptor(null);
                descriptor.init(plugin, child);
                plugin.addModuleDescriptor(descriptor);
            }
        }
        return plugin;
    }

    /**
     * Load mock data for a module descriptor from an XML file.
     * @param filename The XML file of the classpath
     * @return The element containing the module descriptor
     * @throws DocumentException In case of parse errors
     */
    private Element loadModuleDescriptor(final String filename) throws DocumentException {
        SAXReader reader = new SAXReader();
        InputStream is = this.getClass().getClassLoader().getResourceAsStream(filename);
        Document doc = reader.read(is);
        IOUtils.closeQuietly(is);
        return doc.getRootElement();
    }
}
