/**   Copyright 2009 Jeroen Benckhuijsen
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package com.atlassian.confluence.ext.code.languages.impl;

import static org.mockito.Mockito.when;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.atlassian.confluence.ext.code.descriptor.BrushDefinition;
import com.atlassian.confluence.ext.code.descriptor.DescriptorFacade;
import com.atlassian.confluence.ext.code.util.Constants;

/**
 * Provides mocking of the descriptor facade to load the built-in languages
 * for testing purposes.
 * @author Jeroen Benckhuijsen
 *
 */
public final class LanguageDescriptorFacadeMock {
    
    private static final String WEB_RESOURCE = Constants.PLUGIN_KEY + ":syntaxhighlighter-brushes"; 

    private static final BrushDefinition[] BUILTIN_LANGUAGES = {
        new BrushDefinition("sh/scripts/shBrushAS3.js", WEB_RESOURCE),
        new BrushDefinition("sh/scripts/shBrushBash.js", WEB_RESOURCE),
        new BrushDefinition("sh/scripts/shBrushColdFusion.js", WEB_RESOURCE),
        new BrushDefinition("sh/scripts/shBrushCpp.js", WEB_RESOURCE),
        new BrushDefinition("sh/scripts/shBrushCss.js", WEB_RESOURCE),
        new BrushDefinition("sh/scripts/shBrushDelphi.js", WEB_RESOURCE),
        new BrushDefinition("sh/scripts/shBrushDiff.js", WEB_RESOURCE),
        new BrushDefinition("sh/scripts/shBrushErlang.js", WEB_RESOURCE),
        new BrushDefinition("sh/scripts/shBrushGroovy.js", WEB_RESOURCE),
        new BrushDefinition("sh/scripts/shBrushJava.js", WEB_RESOURCE),
        new BrushDefinition("sh/scripts/shBrushJavaFX.js", WEB_RESOURCE),
        new BrushDefinition("sh/scripts/shBrushJScript.js", WEB_RESOURCE),
        new BrushDefinition("sh/scripts/shBrushPerl.js", WEB_RESOURCE),
        new BrushDefinition("sh/scripts/shBrushPhp.js", WEB_RESOURCE),
        new BrushDefinition("sh/scripts/shBrushPlain.js", WEB_RESOURCE),
        new BrushDefinition("sh/scripts/shBrushPowerShell.js", WEB_RESOURCE),
        new BrushDefinition("sh/scripts/shBrushPython.js", WEB_RESOURCE),
        new BrushDefinition("sh/scripts/shBrushRuby.js", WEB_RESOURCE),
        new BrushDefinition("sh/scripts/shBrushScala.js", WEB_RESOURCE),
        new BrushDefinition("sh/scripts/shBrushSql.js", WEB_RESOURCE),
        new BrushDefinition("sh/scripts/shBrushVb.js", WEB_RESOURCE),
        new BrushDefinition("sh/scripts/shBrushXml.js", WEB_RESOURCE)
    };
    
    @Mock
    public DescriptorFacade descriptorFacade;
    
    /**
     * 
     */
    public LanguageDescriptorFacadeMock() {
        super();
        MockitoAnnotations.initMocks(this);
        when(descriptorFacade.listBuiltinBrushes()).thenReturn(BUILTIN_LANGUAGES);
    }
}
